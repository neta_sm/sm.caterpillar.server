import unittest
from fractions import Fraction

from picamera import PiCamera
from time import sleep
from tornado.gen import sleep as coro_sleep

from six import BytesIO

from sm.caterpillar.server.control.camera import CameraDriver, StillSequenceEntry
from tornado.testing import AsyncTestCase, gen_test
from tornado.gen import sleep

from sm.caterpillar.server.initialization import Control


class gpio(unittest.TestCase):
    def test(self):
        cd = CameraDriver.instance()
        for i in cd.CAMERAS:
            cd.switch_to(i)
            with PiCamera() as camera:
                camera.start_recording("/tmp/camera_%d.h264" % i)
                camera.wait_recording(10)
                camera.stop_recording()


class stream(AsyncTestCase):
    @gen_test(timeout=20)
    def test(self):
        cd = CameraDriver.instance()
        with open("/tmp/test_stream.n264", "wb") as fd:
            try:
                cd.tap_feed_data(fd)
                cd.set_video_feed(camera=1, active=True)
                yield sleep(1)
                cd.switch_to(cd.CAMERA_2)
                yield sleep(1)
                cd.switch_to(cd.CAMERA_3)
                yield sleep(1)
                cd.switch_to(cd.CAMERA_4)
                yield sleep(1)
                cd.switch_to(cd.CAMERA_5)
                yield sleep(1)
            finally:
                    yield cd.stop_feed()


class sequence(AsyncTestCase):
    @gen_test(timeout=120)
    def test(self):
        cd = CameraDriver.instance()
        control = Control.instance()
        leds = control.leds
        power = control.power

        class Seqitem(StillSequenceEntry):
            def __init__(self, cameraNo, led_mask, pulses=1, delay_us=0, duration_us=1000):
                self.cameraNo = cameraNo
                self.led_mask = led_mask
                self.pulses = pulses
                self.delay_us = delay_us
                self.duration_us = duration_us

            def __enter__(self):
                cd.switch_to(self.cameraNo)
                leds.pulse(self.led_mask, self.pulses, self.delay_us, self.duration_us)

            def get_stream(self):
                return "/tmp/capture_cam%d.jpg" % self.cameraNo

        def sequence():
            steps = [
                Seqitem(CameraDriver.CAMERA_1, 0b10011),
                Seqitem(CameraDriver.CAMERA_2, 0b00111),
                Seqitem(CameraDriver.CAMERA_3, 0b01110),
                Seqitem(CameraDriver.CAMERA_4, 0b11100),
                Seqitem(CameraDriver.CAMERA_5, 0b11001),
            ]
            for i in steps:
                yield i

        try:
            power.set_12v_power_rail(True)
            leds.set(0x0)
            cd.set_video_feed(sequence=sequence(), active=1, camera=1, framerate=Fraction(1, 1))
            yield cd.disengage.wait()
        finally:
            power.set_12v_power_rail(False)


if __name__ == '__main__':
    unittest.main()
