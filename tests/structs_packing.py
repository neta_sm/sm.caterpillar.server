from ctypes import BigEndianStructure, Union, c_uint8, c_uint16, c_uint32, c_int16, sizeof, memmove, addressof, Structure


class BaseCommand(BigEndianStructure):
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
    ]

if __name__ == '__main__':
    import sys
    data = b"\x01\x00\x02\x03\x04\x05\x06"
    cmd = BaseCommand()
    memmove(addressof(cmd), data, len(data))
    print(cmd.id)
    print(cmd.len)
    print(cmd.seq, "0x%08x" % cmd.seq)
    print(sys.byteorder)
    sys.exit()
