import unittest
from sm.caterpillar.server.initialization import Control
from time import sleep

class Test(unittest.TestCase):
    def test(self):
        control = Control.instance()
        try:
            control.power.set_12v_power_rail(True)
            for j in range(10):
                for i in range(0x100000+1):
                    #print("SET : 0x%02x" % i)
                    control.leds.set(i&0b1111)
                    sleep(0.001)
        finally:
            control.power.set_12v_power_rail(False)

if __name__ == '__main__':
    unittest.main()
