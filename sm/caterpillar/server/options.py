from os import environ
from tornado.options import define, options

DEFAULT_BOARD_REV = int(environ.get("BOARD_REVISION", "2"))
define("board_revision", type=int, default=DEFAULT_BOARD_REV, help="(untested) support for older board revisions")

class _CliOptions:
    @property
    def board_revision(self):
        return options.board_revision

option = _CliOptions()
