from fractions import Fraction
import time
from io import BytesIO
from tornado.concurrent import TracebackFuture

from sm.caterpillar.server.control.camera import StillSequenceEntry, DEFAULT_SETTLE_TIME

PROTOCOL_VERSION = (0, 3)

from tornado.gen import coroutine, sleep, Return
from ctypes import BigEndianStructure, Union, c_uint8, c_uint16, c_uint32, c_int16, sizeof
from json import dumps
from struct import pack, unpack
import logging
log = logging.getLogger(__name__)

RESPONSE_FLAG = 0x80


class ReprBEStructure(BigEndianStructure):
    def __repr__(self):
        d = {}
        for k, v in self._fields_:
            d[k] = getattr(self, k)
        return dumps(d, indent=4, sort_keys=True)

    __str__ = __repr__

    def toData(self):
        raise NotImplementedError


class BaseCommand(ReprBEStructure):
    _pack_ = 1
    _fields_ = (
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
    )

    def _get_response_i(self):
        return BaseResponse()

    def get_response(self):
        res = self._get_response_i()
        res.len = 0
        res.id = self.id | RESPONSE_FLAG
        res.seq = self.seq
        return res

    PACK_FORMAT = "!BHL"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq = unpack(self.PACK_FORMAT, data)
        return self


class BaseResponse(ReprBEStructure):
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("ok", c_uint8),
    ]

    PACK_FORMAT = "!BHLB"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.ok)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.ok = unpack(self.PACK_FORMAT, data)
        return self


class GetVersion(BaseCommand):
    CMD_ID = 0x10
    _pack_ = 1

    @coroutine
    def __call__(self, server):
        log.info("got protocol version query")
        res = self.get_response()
        res.version_major, res.version_minor = PROTOCOL_VERSION
        log.info("version is %r.%r", *PROTOCOL_VERSION)
        return res

    def _get_response_i(self):
        return GetVersion_Response()


class GetVersion_Response(BaseResponse):
    CMD_ID = GetVersion.CMD_ID | RESPONSE_FLAG
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("version_major", c_uint8),
        ("version_minor", c_uint8),
    ]

    PACK_FORMAT = "!BHLBB"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.version_major, self.version_minor)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.version_major, self.version_minor = unpack(self.PACK_FORMAT, data)
        return self


class InitCommand(BaseCommand):
    CMD_ID = 0x01
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("driver", c_uint8),
        ("accel_ramp", c_uint16),
        ("decel_ramp", c_uint16),
        ("max_rpm", c_uint16),
    ]

    def _reply_async(self, future, res_future):
        res = self.get_response()
        try:
            future.result()
            res.ok = 0x01
        except:
            log.exception("error executing command %s" % self)
            res.ok = 0x00
        res_future.set_result(res)

    def __call__(self, server):
        log.info("initialize driver %r, accel_ramp=0x%04x, decel_ramp=0x%04x, max_rpm=%d",
                  self.driver, self.accel_ramp, self.decel_ramp, self.max_rpm)
        future = TracebackFuture()
        res_future = TracebackFuture()
        future.add_done_callback(lambda x: self._reply_async(x, res_future))
        server.machine.initialize_driver(self.driver+1, self.accel_ramp, self.decel_ramp, self.max_rpm, future=future)
        return res_future

    PACK_FORMAT = "!BHLBHHH"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.driver, self.accel_ramp,
                    self.decel_ramp, self.max_rpm)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.driver, self.accel_ramp, \
                    self.decel_ramp, self.max_rpm = unpack(self.PACK_FORMAT, data)
        return self


class EnableCommand(BaseCommand):
    CMD_ID = 0x02
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("driver", c_uint8),
        ("active", c_uint8),
    ]

    def _reply_async(self, future, res_future):
        res = self.get_response()
        try:
            future.result()
            res.ok = 0x01
        except:
            log.exception("error executing command %s" % self)
            res.ok = 0x00
        res_future.set_result(res)

    def __call__(self, server):
        opname = "enable" if self.active else "disable"
        log.info("%s driver %r",
                  self.driver, opname)
        future = TracebackFuture()
        res_future = TracebackFuture()
        future.add_done_callback(lambda x: self._reply_async(x, res_future))
        server.machine.enable_driver(self.driver+1, self.active, future=future)
        return res_future

    PACK_FORMAT = "!BHLBB"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.driver, self.active)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.driver, self.active = unpack(self.PACK_FORMAT, data)
        return self


class SetSpeed(BaseCommand):
    CMD_ID = 0x03
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("driver", c_uint8),
        ("speed", c_int16),
    ]

    def _reply_async(self, future, res_future):
        res = self.get_response()
        try:
            future.result()
            res.ok = 0x01
        except:
            log.exception("error executing command %s" % self)
            res.ok = 0x00
        res_future.set_result(res)

    def __call__(self, server):
        log.debug("set driver of motor %d RPM setpoint = %r",
                  self.driver, self.speed)
        future = TracebackFuture()
        res_future = TracebackFuture()
        future.add_done_callback(lambda x: self._reply_async(x, res_future))
        server.machine.set_driver_rpm(self.driver+1, self.speed, future=future)
        return res_future

    PACK_FORMAT = "!BHLBh"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.driver, self.speed)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.driver, self.speed = unpack(self.PACK_FORMAT, data)
        return self


class GetSpeed(BaseCommand):
    CMD_ID = 0x04
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("driver", c_uint8),
    ]

    def _reply_async(self, future, res_future):
        res = self.get_response()
        try:
            res.speed = future.result()
            res.ok = 0x01
        except:
            log.exception("error executing command %s" % self)
            res.ok = 0x00
        res_future.set_result(res)

    def __call__(self, server):
        log.debug("reading driver %d reported rotational speed",
                  self.driver)
        future = TracebackFuture()
        res_future = TracebackFuture()
        future.add_done_callback(lambda x: self._reply_async(x, res_future))
        server.machine.get_driver_rpm(self.driver+1, future=future)
        return res_future

    PACK_FORMAT = "!BHLB"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.driver)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.driver = unpack(self.PACK_FORMAT, data)
        return self

    def _get_response_i(self):
        return GetSpeed_Response()


class GetSpeed_Response(BaseResponse):
    CMD_ID = GetSpeed.CMD_ID | RESPONSE_FLAG
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("ok", c_uint8),
        ("speed", c_int16),
    ]

    PACK_FORMAT = "!BHLBh"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.ok, self.speed)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.ok, self.speed = unpack(self.PACK_FORMAT, data)
        return self


class Power12VRailEnable(BaseCommand):
    CMD_ID = 0x05
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("active", c_uint8),
    ]

    def _reply_async(self, future, res_future):
        res = self.get_response()
        try:
            future.result()
            res.ok = 0x01
        except:
            log.exception("error executing command %s" % self)
            res.ok = 0x00
        res_future.set_result(res)

    def __call__(self, server):
        opname = "enable" if self.active else "disable"
        log.info("%s 12V power rail",
                  opname)
        future = TracebackFuture()
        res_future = TracebackFuture()
        future.add_done_callback(lambda x: self._reply_async(x, res_future))
        server.machine.rail_12v_enable(self.active, future=future)
        return res_future

    PACK_FORMAT = "!BHLB"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.active)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.active = unpack(self.PACK_FORMAT, data)
        return self


class SetVideoFeed(BaseCommand):
    CMD_ID = 0x06
    _pack_ = 1
    _fields_ = [
        ("id",              c_uint8),
        ("len",             c_uint16),
        ("seq",             c_uint32),
        ("active",          c_uint8),
        ("camera",          c_uint8),
        ("flip_xy",         c_uint8),
        ("auto_mode",       c_uint8),
        ("width",           c_uint16),
        ("height",          c_uint16),
        ("fps_num",         c_uint8),
        ("fps_den",         c_uint8),
        ("shutter_num",     c_uint8),
        ("shutter_den",     c_uint8),
        ("iso",             c_uint16),
        ("settle_time_s",   c_uint8),
        ("record",          c_uint8),
    ]
    PACK_FORMAT = "!BHLBBBBHHBBBBHBB"

    @coroutine
    def __call__(self, server):
        opname = "enable" if self.active else "disable"
        log.info("%s video feed from camera %r",
                  opname, self.camera)
        res = self.get_response()
        camera = server.machine.camera
        args = {}
        if self.width and self.height:
            args.update(resolution=(self.width, self.height))
        if self.fps_num and self.fps_den:
            args.update(framerate=Fraction(self.fps_num, self.fps_den))
        if self.shutter_num and self.shutter_den:
            args.update(shutter_speed=Fraction(self.shutter_num, self.shutter_den))
        if self.iso != 0xff:
            args.update(iso=self.iso)
        if self.settle_time_s != 0xff:
            args.update(settle_time=self.settle_time_s)
        if self.camera:
            args.update(camera=self.camera)
        feed_fd = None
        if self.record:
            if not server.is_video_recording():
                feed_fd = server.start_video_recording()
        else:
            if server.is_video_recording():
                server.stop_video_recording()


        try:
            camera.set_video_feed(
                active=self.active,
                flip_vert=self.flip_xy & 0x1,
                flip_horiz=self.flip_xy & 0x2,
                auto_mode=self.auto_mode,
                **args
            )
            if feed_fd:
                camera.tap_feed_data(feed_fd)

            res.ok = 0x01
        except:
            log.exception("unable to set video feed status")
            res.ok = 0x00
        return res

    def toData(self):
        return pack(self.PACK_FORMAT,
                    self.id, self.len, self.seq,
                    self.active, self.camera,
                    self.flip_xy,
                    self.auto_mode,
                    self.width, self.height,
                    self.fps_num, self.fps_den,
                    self.shutter_num, self.shutter_den,
                    self.iso, self.settle_time_s, self.record)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, \
            self.active, self.camera, \
            self.flip_xy, \
            self.auto_mode, \
            self.width, self.height, \
            self.fps_num, self.fps_den, \
            self.shutter_num, self.shutter_den, \
            self.iso, self.settle_time, self.record \
            = unpack(self.PACK_FORMAT, data)
        return self


class SetCameraLED(BaseCommand):
    CMD_ID = 0x07
    _pack_ = 1
    _fields_ = [
        ("id", c_uint8),
        ("len", c_uint16),
        ("seq", c_uint32),
        ("on_mask", c_uint32),
    ]

    @coroutine
    def __call__(self, server):
        log.info("setting LEDs enable mask = 0x%04x",
                  self.on_mask)
        res = self.get_response()
        leds = server.machine.leds

        try:
            leds.set(self.on_mask)
            res.ok = 0x01
        except:
            res.ok = 0x00
        return res

    PACK_FORMAT = "!BHLL"

    def toData(self):
        return pack(self.PACK_FORMAT, self.id, self.len, self.seq, self.on_mask)

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id, self.len, self.seq, self.on_mask = unpack(self.PACK_FORMAT, data)
        return self


class ShotStillSequence(BaseCommand):
    CMD_ID = 0x08
    _pack_ = 1
    _fields_ = [
        ("id",                  c_uint8),
        ("len",                 c_uint16),
        ("seq",                 c_uint32),
        ("mode",                c_uint8),
        ("auto_mode",           c_uint8),
        ("width",               c_uint16),
        ("height",              c_uint16),
        ("fps_num",             c_uint8),
        ("fps_den",             c_uint8),
        ("shutter_num",         c_uint8),
        ("shutter_den",         c_uint8),
        ("iso",                 c_uint16),
        ("settle_time_s",       c_uint8),
        ("format",              c_uint8),
        ("camera0",             c_uint8),
        ("camera0_flip_xy", c_uint8),
        ("camera0_leds",        c_uint16),
        ("camera1",             c_uint8),
        ("camera1_flip_xy", c_uint8),
        ("camera1_leds",        c_uint16),
        ("camera2",             c_uint8),
        ("camera2_flip_xy", c_uint8),
        ("camera2_leds",        c_uint16),
        ("camera3",             c_uint8),
        ("camera3_flip_xy", c_uint8),
        ("camera3_leds",        c_uint16),
        ("camera4",             c_uint8),
        ("camera4_flip_xy", c_uint8),
        ("camera4_leds",        c_uint16),
        ("camera5",             c_uint8),
        ("camera5_flip_xy", c_uint8),
        ("camera5_leds",        c_uint16),
        ("camera6",             c_uint8),
        ("camera6_flip_xy", c_uint8),
        ("camera6_leds",        c_uint16),
        ("camera7",             c_uint8),
        ("camera7_flip_xy", c_uint8),
        ("camera7_leds",        c_uint16),
        ("led_pulses",          c_uint8),
        ("led_pulse_len_ms",    c_uint16),
        ("led_pulse_del_ms",    c_uint16),
    ]

    PACK_FORMAT = "!BHLBBHHBBBBHBBBBHBBHBBHBBHBBHBBHBBHBBHBHH"


    @coroutine
    def __call__(self, server):
        log.info("starting still shoot sequence")
        res = self.get_response()
        leds = server.machine.leds
        camera = server.machine.camera

        args = {}
        if self.width and self.height:
            args.update(resolution=(self.width, self.height))
        if self.fps_num and self.fps_den:
            args.update(framerate=Fraction(self.fps_num, self.fps_den))
        if self.shutter_num and self.shutter_den:
            args.update(shutter_speed=Fraction(self.shutter_num, self.shutter_den))
        if self.iso != 0xff:
            args.update(iso=self.iso)
        if self.settle_time_s != 0xff:
            args.update(settle_time=self.settle_time_s)
        if self.format == 0x01:
            args.update(format="rgb")

        pulses, delay_us, duration_us = self.led_pulses, self.led_pulse_del_ms*1000, self.led_pulse_len_ms*1000
        use_pulses = self.mode == 0x03

        class Step(StillSequenceEntry):
            def __init__(self, still_nr, camera, flip_xy, led_mask):
                self.still_nr = still_nr
                self.camera = camera
                self.flip_xy = flip_xy
                self.led_mask = led_mask
                self.ok = False
                self.buf = BytesIO()

            def enter(self):
                log.debug("ENTER: snapshot of still photo #%d from camera %d", self.still_nr, self.camera)
                if self.camera:
                    camera.switch_to(self.camera)
                    camera.set_caption("cam%d" % (self.camera+1))

                    if use_pulses:
                        leds.pulse(self.led_mask, pulses, delay_us, duration_us)
                    else:
                        leds.set(self.led_mask)
                    self.ok = True
                return self.ok

            def get_stream(self):
                if self.ok:
                    log.info("SNAP: snapshot of still photo #%d from camera %d", self.still_nr, self.camera)
                    return self.buf

            def exit(self, *a, **ka):
                if self.ok:
                    log.debug("EXIT: snapshot of still photo #%d from camera %d", self.still_nr, self.camera)
                    leds.set(0x00)
                    self.buf.seek(0)
                    server.stills[self.camera] = self.buf.read()

        sequence = [
            Step(1, self.camera0, self.camera0_flip_xy, self.camera0_leds),
            Step(2, self.camera1, self.camera1_flip_xy, self.camera1_leds),
            Step(3, self.camera2, self.camera2_flip_xy, self.camera2_leds),
            Step(4, self.camera3, self.camera3_flip_xy, self.camera3_leds),
            Step(5, self.camera4, self.camera4_flip_xy, self.camera4_leds),
            Step(6, self.camera5, self.camera5_flip_xy, self.camera5_leds),
            Step(7, self.camera6, self.camera6_flip_xy, self.camera6_leds),
            Step(8, self.camera7, self.camera7_flip_xy, self.camera7_leds),
        ]

        long_exposure = self.mode == 0x03
        use_video_port = self.mode == 0x02

        try:
            server.clear_stills()
            camera.shoot_sequence(
                sequence=sequence,
                long_exposure=long_exposure,
                use_video_port=use_video_port,
                auto_mode=self.auto_mode,
                **args
            )
            yield sleep(1)
            yield camera.disengage.wait()
            ext = ".jpg"
            if self.format == 0x01:
                if self.width and self.height:
                    ext = "_%04dx%04d.rgb" % (self.width, self.height)
                else:
                   ext = ".rgb"
            server.save_stills(ext=ext)
            res.ok = 0x01
        except:
            res.ok = 0x00
        raise Return(res)

    def toData(self):
        return pack(self.PACK_FORMAT,
            self.id                 ,
            self.len                ,
            self.seq                ,
            self.mode          ,
            self.auto_mode          ,
            self.width              ,
            self.height             ,
            self.fps_num            ,
            self.fps_den            ,
            self.shutter_num        ,
            self.shutter_den        ,
            self.iso                ,
            self.settle_time_s      ,
            self.format             ,
            self.camera0            ,
            self.camera0_flip_xy,
            self.camera0_leds       ,
            self.camera1            ,
            self.camera1_flip_xy,
            self.camera1_leds       ,
            self.camera2            ,
            self.camera2_flip_xy,
            self.camera2_leds       ,
            self.camera3            ,
            self.camera3_flip_xy,
            self.camera3_leds       ,
            self.camera4            ,
            self.camera4_flip_xy,
            self.camera4_leds       ,
            self.camera5            ,
            self.camera5_flip_xy,
            self.camera5_leds       ,
            self.camera6            ,
            self.camera6_flip_xy,
            self.camera6_leds       ,
            self.camera7            ,
            self.camera7_flip_xy,
            self.camera7_leds       ,
            self.led_pulses         ,
            self.led_pulse_len_ms   ,
            self.led_pulse_del_ms   ,
            )

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id                 , \
        self.len                , \
        self.seq                , \
        self.mode          , \
        self.auto_mode          , \
        self.width              , \
        self.height             , \
        self.fps_num            , \
        self.fps_den            , \
        self.shutter_num        , \
        self.shutter_den        , \
        self.iso                , \
        self.settle_time_s      , \
        self.format             , \
        self.camera0            , \
        self.camera0_flip_xy, \
        self.camera0_leds       , \
        self.camera1            , \
        self.camera1_flip_xy, \
        self.camera1_leds       , \
        self.camera2            , \
        self.camera2_flip_xy, \
        self.camera2_leds       , \
        self.camera3            , \
        self.camera3_flip_xy, \
        self.camera3_leds       , \
        self.camera4            , \
        self.camera4_flip_xy, \
        self.camera4_leds       , \
        self.camera5            , \
        self.camera5_flip_xy, \
        self.camera5_leds       , \
        self.camera6            , \
        self.camera6_flip_xy, \
        self.camera6_leds       , \
        self.camera7            , \
        self.camera7_flip_xy, \
        self.camera7_leds       , \
        self.led_pulses         , \
        self.led_pulse_len_ms   , \
        self.led_pulse_del_ms   , \
        = unpack(self.PACK_FORMAT, data)
        return self


class LEDPulse(BaseCommand):
    CMD_ID = 0x09
    _pack_ = 1
    _fields_ = [
        ("id",                  c_uint8),
        ("len",                 c_uint16),
        ("seq",                 c_uint32),
        ("led_mask",            c_uint16),
        ("led_pulses",          c_uint8),
        ("led_pulse_len_ms",    c_uint16),
        ("led_pulse_del_ms",    c_uint16),
    ]

    PACK_FORMAT = "!BHLHBHH"

    @coroutine
    def __call__(self, server):
        log.info("executing LED pulse loop with enable mask = 0x%04x", self.led_mask)
        res = self.get_response()
        leds = server.machine.leds

        try:
            leds.pulse(self.led_mask, self.led_pulses, self.led_pulse_del_ms*1000, self.led_pulse_len_ms*1000)
            res.ok = 0x01
        except:
            res.ok = 0x00
        return res

    def toData(self):
        return pack(self.PACK_FORMAT,
            self.id                 ,
            self.len                ,
            self.seq                ,
            self.led_mask           ,
            self.led_pulses         ,
            self.led_pulse_len_ms   ,
            self.led_pulse_del_ms   ,
            )

    @classmethod
    def fromData(cls, data):
        self = cls()
        self.id                 , \
        self.len                , \
        self.seq                , \
        self.led_mask           , \
        self.led_pulses         , \
        self.led_pulse_len_ms   , \
        self.led_pulse_del_ms   , \
        = unpack(self.PACK_FORMAT, data)
        return self



class Command:
    getters = {
        GetVersion.CMD_ID:  GetVersion,
        EnableCommand.CMD_ID: EnableCommand,
        InitCommand.CMD_ID: InitCommand,
        SetSpeed.CMD_ID: SetSpeed,
        GetSpeed.CMD_ID: GetSpeed,
        Power12VRailEnable.CMD_ID: Power12VRailEnable,
        SetVideoFeed.CMD_ID: SetVideoFeed,
        SetCameraLED.CMD_ID: SetCameraLED,
        LEDPulse.CMD_ID: LEDPulse,
        ShotStillSequence.CMD_ID: ShotStillSequence,
    }

    @classmethod
    def fromData(cls, data):
        if not data:
            return
        cmd_id = data[0]
        ccls = cls.getters.get(cmd_id)
        if ccls is None:
            raise RuntimeError("unknown command id 0x%02x" % cmd_id)
        return ccls.fromData(data)


class Response(Union):
    getters = {
        GetSpeed_Response.CMD_ID: GetSpeed_Response,
        GetVersion_Response.CMD_ID: GetVersion_Response,
    }

    @classmethod
    def fromData(cls, data):
        if not data:
            return
        cmd_id = data[0]
        ccls = cls.getters.get(cmd_id)
        if ccls is None:
            ccls = BaseResponse
        return ccls.fromData(data)


class Factory:
    def __init__(self):
        self.ctr = -1

    def __call__(self, cmd_class, **kwargs):
        self.ctr += 1
        res = cmd_class()
        res.id = cmd_class.CMD_ID
        res.len = sizeof(res)
        res.seq = self.ctr
        for k, v in kwargs.items():
            setattr(res, k, v)
        return res