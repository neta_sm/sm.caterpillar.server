import logging

from tornado.ioloop import PeriodicCallback
from tornado.options import define, options

from . import StatusDispatcher
from .commons import DEF_DRIVER_ARAMP, DEF_DRIVER_MAXRPM, DEF_DRIVER_DRAMP
from .initialization import Control
from os import environ

log = logging.getLogger(__name__)

DRIVERES_UPDATE_MS = int(environ.get("DRIVERES_UPDATE_MS", 100))
define("driveres_update_ms", type=int, default=DRIVERES_UPDATE_MS, help="DriverES update rate (millisecs)")

class DriverError(Exception): pass
class DriverStatusError(DriverError): pass


class DriverStatus(StatusDispatcher):
    def __init__(self, engine_n):
        super(DriverStatus, self).__init__()
        self.engine_n = engine_n
        self.control = Control.instance()
        self._powered = False
        self._reset_cached_status()

    def _reset_cached_status(self):
        self._initialized = False
        self._enabled = False
        self._rpm_setpoint = 0

    def _check_powered(self):
        if not self._powered:
            raise DriverStatusError("can't access driver for motor %d: not currently powered" % self.engine_n)

    def initialize(self, aramp=None, dramp=None, max_rpm=None):
        self._check_powered()
        if self._enabled:
            raise DriverStatusError("can't initialize: driver for motor %d currently enabled. disable it first" % self.engine_n)
        self.control.modbus.driver_init(self.engine_n-1, aramp, dramp, max_rpm)

        if not self._initialized:
            self._initialized = True
            self._notify_status_change()

    def enable(self, active=True):
        if active:
            self._check_powered()
        if not self._initialized and active:
            raise DriverStatusError("can't enable: driver for motor %d not initialized" % self.engine_n)

        if self._enabled != active:
            self.control.modbus.driver_enable(self.engine_n-1, active)
            self._enabled = active
            if not active:
                self._rpm_setpoint = 0
            self._notify_status_change()

    def disable(self):
        self.enable(False)

    def write_current_setpoint(self):
        self._check_powered()
        if not self._enabled:
            raise DriverStatusError("can't set RPM: driver for motor %d not enabled" % self.engine_n)
        self.control.modbus.set_rpm(self.engine_n-1, self._rpm_setpoint)

    def set_rpm(self, speed):
        if speed == self._rpm_setpoint:
            log.debug("skipping command send: setpoint unchanged (%r)", speed)
            return
        self._check_powered()
        if not self._enabled:
            raise DriverStatusError("can't set RPM: driver for motor %d not enabled" % self.engine_n)
        self._rpm_setpoint = speed

    def get_rpm_actual(self):
        self._check_powered()
        if not self._enabled:
            raise DriverStatusError("can't read current RPM: driver for motor %d not enabled" % self.engine_n)
        return self.control.modbus.read_rpm(self.engine_n-1)

    def get_rpm_setpoint(self):
        return self._rpm_setpoint

    def on_power_status_change(self, power):
        self._powered = power.rail_12v_on()
        if not self._powered:
            self._reset_cached_status()


class PowerStatus(StatusDispatcher):
    def __init__(self):
        super(PowerStatus, self).__init__()
        self.control = Control.instance()
        self._12v_rail_on = False

    def rail_12v_enable(self, active):
        if self._12v_rail_on != active:
            self.control.power.set_12v_power_rail(active)
            self._12v_rail_on = active
            self._notify_status_change()

    def rail_12v_disable(self):
        self.rail_12v_enable(False)

    def rail_12v_on(self):
        return self._12v_rail_on


M1 = 1
M2 = 2
M3 = 3
M4 = 4


class MachineException(Exception): pass


class MachineStatus:

    def __init__(self):
        self.motors = {
            M1: DriverStatus(M1),
            M2: DriverStatus(M2),
            M3: DriverStatus(M3),
            M4: DriverStatus(M4),
        }
        self.power = PowerStatus()
        for motor in self.motors.values():
            self.power.watched_by(motor.on_power_status_change)

        StatusDispatcher.broadcast_all()  # everyone tell everyone about their status
        self.power.rail_12v_enable(False) # start ALL OFF

        if options.driveres_update_ms:
            self.update_motors = PeriodicCallback(self._on_update_motors, options.driveres_update_ms)
            self.update_motors.start()
        self.control = Control.instance()
        self.leds = self.control.leds
        self.camera = self.control.camera

    def _select_motor(self, n):
        try:
            return self.motors[n]
        except KeyError:
            raise MachineException("motor %r not found. valid motors are {%s}" % (
                n, ", ".join(str(i) for i in self.motors)
            ))

    def initialize_driver(self, n, aramp=None, dramp=None, max_rpm=None, future=None):
        try:
            aramp = aramp if aramp else DEF_DRIVER_ARAMP
            dramp = dramp if dramp else DEF_DRIVER_DRAMP
            max_rpm = max_rpm if max_rpm else DEF_DRIVER_MAXRPM

            self._select_motor(n).initialize(aramp=aramp, dramp=dramp, max_rpm=max_rpm)
            if future:
                future.set_result(None)
        except Exception as err:
            if future:
                future.set_exception(err)
            else:
                raise

    def enable_driver(self, n, active, future=None):
        try:
            self._select_motor(n).enable(active)
            if future:
                future.set_result(None)
        except Exception as err:
            if future:
                future.set_exception(err)
            else:
                raise

    def set_driver_rpm(self, n, speed, future=None):
        try:
            self._select_motor(n).set_rpm(speed)
            if future:
                future.set_result(None)
        except Exception as err:
            if future:
                future.set_exception(err)
            else:
                raise

    def get_driver_rpm(self, n, future=None):
        try:
            res = self._select_motor(n).get_rpm_actual()
            if future:
                future.set_result(res)
            return res
        except Exception as err:
            if future:
                future.set_exception(err)
            else:
                raise

    def rail_12v_enable(self, active, future=None):
        try:
            self.power.rail_12v_enable(active)
            if future:
                future.set_result(None)
        except Exception as err:
            if future:
                future.set_exception(err)
            else:
                raise

    def _on_update_motors(self):
        for mid, m in self.motors.items():
            try:
                m.write_current_setpoint()
            except DriverError as err:
                log.debug("unable to set rpm for motor %r: %s", mid, str(err))
                pass
            except:
                log.exception("unable to set rpm for motor %r", mid)