# methods that a PowerDriver descendant must override:
from tornado.options import define

define("noop", type=bool, default=False)
define("no_modbus_read", type=bool, default=False)

class AbstractPowerDriver:
    def set_battery_charge(self, on):
        raise NotImplementedError

    def set_5v_power_rail(self, on):
        raise NotImplementedError

    def set_12v_power_rail(self, on):
        raise NotImplementedError
