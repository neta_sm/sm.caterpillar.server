from tornado.options import define, options
from sm.caterpillar.server.commons import SLAVE_ADDR1, SLAVE_ADDR2, BASE_ADDR_MOT1, BASE_ADDR_MOT2, NPOLE, OFFSET_PAR
from sm.caterpillar.utils.singleton import singleton
from ..commons import DEF_DRIVER_MAXRPM
from time import sleep
from collections import defaultdict
from os import environ

from sm.caterpillar.utils.libmodbus import Bus

import logging
log = logging.getLogger(__name__)

DRIVES_PORT = environ.get("DRIVERES_PORT", "/dev/ttyUSB0")
define("driveres_port", default=DRIVES_PORT, type=str, help="serial port for modbus")

DRIVES_BRATE = int(environ.get("DRIVES_BRATE", 115200))
define("driveres_brate", default=DRIVES_BRATE, type=int, help="serial port baud rate")
define("driveres_parity", default="N", type=str, help="serial port parity [N:none, E:even, O:odd]")
define("driveres_dbits", default=8, type=int, help="serial port data bits")
define("driveres_sbits", default=1, type=int, help="serial port stop bits")

MAX_RPM_ADDR = OFFSET_PAR + 0x0006
ARAMP_ADDR = 0x000C
DRAMP_ADDR = 0x000D
CONTROL_MODE_ADDR = 0x0002
RPM_READ_ADDR = 0x0007
RPM_SET_ADDR = 0x000E
CONTROL_MODE_RPM = 3


class _RegProxy:
    def __init__(self, bus, slave, addr):
        self.bus = bus
        self.slave = slave
        self.addr = addr

    def set(self, val, offset=0):
        self.bus.write(self.slave, self.addr+offset, val)

    def get(self, offset=0):
        return self.bus.read(self.slave, self.addr+offset)


@singleton
class Modbus:
    MOTORS = 4
    OK_MOTORS = list(range(MOTORS))

    def __init__(self):
        log.info("opening Modbus RTU over serial port %s", options.driveres_port)
        self.bus = Bus(
            port=options.driveres_port,
            baud=options.driveres_brate,
            dbits=options.driveres_dbits,
            parity=options.driveres_parity.upper(),
            sbits=options.driveres_sbits
        )

        self.enable_regs = [
            _RegProxy(self.bus, SLAVE_ADDR1, BASE_ADDR_MOT1),
            _RegProxy(self.bus, SLAVE_ADDR1, BASE_ADDR_MOT2),
            _RegProxy(self.bus, SLAVE_ADDR2, BASE_ADDR_MOT1),
            _RegProxy(self.bus, SLAVE_ADDR2, BASE_ADDR_MOT2),
        ]
        self.maxrpm = defaultdict(lambda: DEF_DRIVER_MAXRPM)

    def driver_enable(self, nmot, on, direct=False):
        if nmot not in self.OK_MOTORS:
            raise RuntimeError("invalid motor id. %r not in %r" % (nmot, self.OK_MOTORS))
        reg = self.enable_regs[nmot]
        if on:
            log.info("enable motor id %r", nmot)
            reg.set(0x0006)
            sleep(0.1)
            reg.set(0x000F)
            sleep(0.1)
        else:
            log.info("disable motor id %r", nmot)
            #val = yield reg.get()
            #if val == 0x000F:
            reg.set(0x0006)
            sleep(0.1)
            reg.set(0x0000)
            sleep(0.1)

    def driver_init(self, nmot, aramp, dramp, max_rpm):
        if nmot not in self.OK_MOTORS:
            raise RuntimeError("invalid motor id. %r not in %r" % (nmot, self.OK_MOTORS))

        reg = self.enable_regs[nmot]
        log.info("initializing driver id %r", nmot)

        self.driver_enable(nmot, False, direct=True)

        value = int(2343750 / (max_rpm * NPOLE))
        reg.set(value, MAX_RPM_ADDR)
        sleep(0.1)
        self.maxrpm[nmot] = max_rpm
        reg.set(aramp, ARAMP_ADDR)
        sleep(0.1)
        reg.set(dramp, DRAMP_ADDR)
        sleep(0.1)
        reg.set(CONTROL_MODE_RPM, CONTROL_MODE_ADDR)
        sleep(0.1)

    def read_rpm(self, nmot):
        if nmot not in self.OK_MOTORS:
            raise RuntimeError("invalid motor id. %r not in %r" % (nmot, self.OK_MOTORS))

        reg = self.enable_regs[nmot]
        max_rpm = self.maxrpm[nmot]

        speed_read = reg.get(RPM_READ_ADDR)
        res = int((speed_read * max_rpm) / 0x10000)
        log.debug("register read for RPM of driver %r: %r --> %r RPM", nmot, speed_read, res)
        return res

    def set_rpm(self, nmot, rpm):
        if nmot not in self.OK_MOTORS:
            raise RuntimeError("invalid motor id. %r not in %r" % (nmot, self.OK_MOTORS))

        reg = self.enable_regs[nmot]
        maxrpm = self.maxrpm[nmot]
        if abs(rpm) > maxrpm:
            #raise RuntimeError("RPM %r is excessive for driver id %r: RPM max = %r" % (rpm, nmot, maxrpm))
            if rpm > maxrpm:
                rpm = maxrpm
            if rpm < -maxrpm:
                rpm = -maxrpm


        val = int((float(rpm) / maxrpm) * 32768)
        reg.set(val, RPM_SET_ADDR)


def test():
    modbus = Modbus()
    modbus.driver_enable(0, False)
    sleep(2)
    modbus.driver_init(0, 0x2e, 0x2e, 8000)
    sleep(2)
    modbus.driver_enable(0, True)
    sleep(2)
    modbus.set_rpm(0, 3000)
    sleep(2)
    modbus.driver_enable(0, False)

if __name__ == '__main__':
    from tornado.ioloop import IOLoop
    from tornado.options import parse_command_line
    parse_command_line()
    loop = IOLoop.instance()
    loop.add_callback(test)
    loop.start()
    """
MODBUS_READ 1 registers at 0x6500:
 - 0x0000
MODBUS_WRITE register at 0x6500, value=0x0000: OK
Insert Max Vel [rpm]:
8000
MODBUS_WRITE register at 0x6586, value=0x0124: OK
Insert Aceleration ramp (typ. 0x000A) [hex]:
2e
MODBUS_WRITE register at 0x650c, value=0x002e: OK
Insert deceleration ramp (typ. 0x000A) [hex]:
2e
MODBUS_WRITE register at 0x650d, value=0x002e: OK
Insert Mode of Operation (1 Posizion, 3 Velocity):
3
Insert command [0 command List]:


[D 160726 23:36:23 libmodbus:82] modbus: write 0x6500@1: 0x0006
[D 160726 23:36:23 libmodbus:61] modbus: set_slave 1
[D 160726 23:36:23 libmodbus:82] modbus: write 0x6500@1: 0x0000

[D 160726 23:36:23 libmodbus:82] modbus: write 0x6586@1: 0x0124
[D 160726 23:36:24 libmodbus:82] modbus: write 0x658c@1: 0x002e
[D 160726 23:36:24 libmodbus:82] modbus: write 0x658d@1: 0x002e


[D 160726 23:36:24 libmodbus:82] modbus: write 0x6502@1: 0x0003
MODBUS_WRITE register at 0x6502, value=0x0003: OK


        """
