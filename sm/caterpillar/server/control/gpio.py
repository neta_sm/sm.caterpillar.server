try:
    import RPi.GPIO as GPIO
except:
    class GPIO:
        @staticmethod
        def setwarnings(*a, **ka):
            pass
        @staticmethod
        def setmode(*a, **ka):
            pass
        @staticmethod
        def setup(*a, **ka):
            pass
        @staticmethod
        def output(*a, **ka):
            pass
        BCM=None
        OUT=None
    pass


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
