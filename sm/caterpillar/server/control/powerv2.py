#!/usr/bin/env python
#REV 1.0.0.c
#Copiare tutta la cartella in raspberry
#Abilitare i2c da preference
#controllare che l'indirizzo dello slave sia corretto
#lanciare con sudo python3 BatteryCharge.py
#REV 1.0.0.d
#inserito controllo pin per abilitazione 12V
#REV 1.0.0.e
#inserito comandi De Marinis
#REV 1.0.0.f
#Correzioni MDM
#REV 1.0.0.g
#Importato in caterpillar/driver <a.gronchi>


import logging
import time
from datetime import datetime
from time import sleep

import numpy as np
from tornado.options import options

from sm.caterpillar.utils.i2c import i2c, i2cutils
from . import AbstractPowerDriver
from .gpio import GPIO

log = logging.getLogger(__name__)

SW12 = 27 #Switch 12V

GPIO.setup(SW12,GPIO.OUT)
GPIO.output(SW12, 0)

class Driver(object):
    '''
    Simple Driver implementation for I2C communication (pin6 PS high)
    '''
    I2C_ADDRESS_HIGH = 0x71 # Pin1 is high (default)

    # CMD_5ON     = 0x81     # Power ON 5V Command
    # CMD_5OFF    = 0x80     # Power OFF 5V Command
    # CMD_BC_ON   = 0x41     # Power ON Battery Charge
    # CMD_BC_OFF  = 0x40     # Power OFF Battery Charge
    # CMD_POFF_T  = 0x82     # Power OFF/ON 5V after T*3min
    # CMD_POFF_C  = 0x84     # Power OFF/ON 5V after %C

    CMD_G_KEY   = 0x21     # Write Global Key
    CMD_L_KEY   = 0x22     # Write Local Key
# New Command
    SYSTEM_DATE_CMD    = 0x28     # Data CMD
    CMD_POWER   = 0x30     # System Shut-Down - bring system to minimum power consumption
    CMD_BAT     = 0x31     # Power Battery Charger on/off
    CMD_3V3     = 0x32     # Power 3V3 switch on/off
    CMD_12      = 0x34     # Power 12 switch on/off 
    CMD_LASER   = 0x38     # Switch LASERS on/off
    CMD_5       = 0x40     # Power 5V switch on/off without Delay
    CMD_REB_T   = 0x41     # Switch off 5V DC-DC and power-on after a certain amount of seconds
    CMD_REB_D   = 0x42     # Switch off 5V DC-DC and power-on at an exact date
    CMD_REB_P   = 0x44     # Switch off 5V DC-DC and power-on when battery charge is above Batt%
    CMD_C_STATUS  = 0x10   # Charge Status
    CMD_G_STATUS  = 0x11   # Return a 16 bit indexed value of the  Fuel gauge data
    CMD_B_STATUS  = 0x12   # Return a 16 bit indexed value of the BMS data
 
    # READ_V      = 0x11     # Read Tension Command
    # READ_A      = 0x12     # Read Current Command
    # READ_T      = 0x14     # Read Temperature Command
    # READ_C_100  = 0x15     # Read Gauge % Command
    # READ_C_C    = 0x16     # Read Gauge Coulomb Command

    OFFSET_CURR = 32767

    def __init__(self, bus, address):
        '''
        Constructor
        '''
        # rx = []
        self.bus = bus           # I2C bus interface 0|1 [/dev/i2c-0 /dev/i2c-1]
        self.address = address   # I2C address

        self.Global_key32 = [0xAA, 0x11, 0xBB, 0x00]
        self.Global_key16 = [0xDD, 0x33]
        self.Local_key32 = [0xBB, 0x22, 0xCC, 0x11]
        self.Local_key16 = [0xEE, 0x44]

        self.Temperature = 0
        self.Tension = 0
        self.Current = 0
        self.Charge_100 = 0
        self.Charge_Cou = 0
        self.Charge = 0
        self.codex = 0
        self.System = 0
        
        self.ChargerEpoch = int(datetime.utcnow().timestamp()) # assign system date

        # reset the sensor
        # bus.write_byte(self.address, CMD_RESET)

    def read_data(self, master, cmd_measure):
        # Send command to read
        master.transaction(i2c.writing_bytes(self.address, cmd_measure))
        # Wait for conversion to complete
        #time.sleep(0.001);

        # Request read command
        #master.transaction(i2c.writing_bytes(self.address, Driver.CMD_ADC_READ))
        highByte, lowByte = master.transaction(i2c.reading(self.address, 2))[0]
        result = (highByte << 8) + lowByte
        return result

    def read_index(self, master, cmd_measure, index):
        # Send command and index to read a 16bit
        master.transaction(i2c.writing_bytes(self.address, cmd_measure, index))
        # Wait for conversion to complete
        time.sleep(0.100);

        # Request read command
        #master.transaction(i2c.writing_bytes(self.address, Driver.CMD_ADC_READ))
        highByte, lowByte = master.transaction(i2c.reading(self.address, 2))[0]
        result = (highByte << 8) + lowByte
        return result

    def read_ndata(self, master, cmd_measure, nbytes):
        # Send command to read nbytes
        master.transaction(i2c.writing_bytes(self.address, cmd_measure))
        # Wait for conversion to complete
        #time.sleep(0.001);

        # Request read command
        #master.transaction(i2c.writing_bytes(self.address, Driver.CMD_ADC_READ))
        rx = master.transaction(i2c.reading(self.address, nbytes))[0]
        return rx
        
    def write_command(self, cmd):
        # Send command to write
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, cmd))
            
    def write_cmd_data(self, cmd, data):
        # Send command + data to write
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, cmd, data))
            
    def write_key32(self, cmd, K):
        # Send 2 bytes key code
        #print ('msb: ', self.key[0])
        #print ('lsb: ', self.key[1])
        B0_K = np.uint8(K)
        B1_K = np.uint8(K >> 8)
        B2_K = np.uint8(K >> 16)
        B3_K = np.uint8(K >> 24)
        print('{:X} , {:X}, {:X}, {:X}'.format(B3_K,B2_K,B1_K,B0_K))
        with i2c.I2CMaster(self.bus) as master:
            if (cmd == 'Local'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_L_KEY, B3_K, B2_K, B1_K, B0_K))
            elif (cmd == 'Global'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_G_KEY, B3_K, B2_K, B1_K, B0_K))
            else:
                print ('ERROR: Command wrong:')

    def write_key16(self, cmd, K):
        # Send 2 bytes key code
        B0_K = np.uint8(K)
        B1_K = np.uint8(K >> 8)
        print('{:X} , {:X}'.format(B1_K,B0_K))
        with i2c.I2CMaster(self.bus) as master:
            if (cmd == 'Local'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_L_KEY, B1_K, B0_K))
            elif (cmd == 'Global'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_G_KEY, B1_K, B0_K))
            else:
                print ('ERROR: Command wrong:')

    def Power_off_cmd(self, T):
        # Send 2 bytes key code
        B0_T = np.uint8(T)
        B1_T = np.uint8(T >> 8)
        B2_T = np.uint8(T >> 16)
        B3_T = np.uint8(T >> 24)
        #print('{:X} , {:X}, {:X}, {:X}'.format(B3_T,B2_T,B1_T,B0_T))
        with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_POWER, B3_T, B2_T, B1_T, B0_T))

    def Power_Bat_cmd(self, mode):
        # Send 2 bytes key code
        if mode > 1 or mode < 0:
            print ('wrong command')
        else:
            with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_BAT, np.uint8(mode)))

    def Power_3V3_cmd(self, mode):
        # Send 2 bytes key code
        if mode > 1 or mode < 0:
            print ('wrong command')
        else:
            with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_3V3, np.uint8(mode)))

    def Power_12_cmd(self, mode):
        # Send 2 bytes key code
        if mode > 1 or mode < 0:
            print ('wrong command')
        else:
            with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_12, np.uint8(mode)))

    def Power_5_cmd(self, mode):
        # Send 2 bytes key code
        if mode > 1 or mode < 0:
            print ('wrong command')
        else:
            with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_5, np.uint8(mode)))

    def Power_Laser_cmd(self, mode):
        # Send 2 bytes key code
        if mode > 3 or mode < 0:
            print ('wrong command')
        else:
            with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_LASER, np.uint8(mode)))

    def Set_Date_cmd(self):
        # Send 4 bytes Date code
        self.ChargerEpoch = int(datetime.utcnow().timestamp())
        B0_D = np.uint8(self.ChargerEpoch)
        B1_D = np.uint8(self.ChargerEpoch >> 8)
        B2_D = np.uint8(self.ChargerEpoch >> 16)
        B3_D = np.uint8(self.ChargerEpoch >> 24)
        #print('{:X} , {:X}, {:X}, {:X}'.format(B3_D,B2_D,B1_D,B0_D))
        with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.SYSTEM_DATE_CMD, B3_D, B2_D, B1_D, B0_D))
                # app = master.transaction(i2c.reading(self.address, 4))[0]
                # self.Data = (app[0] << 24) + (app[1] << 16) + (app[2] << 8) + app[3]
        #print('N. of seconds since 01/Jan/1970 = {:d}'.format(self.Data))
        print('Charger UTC date UPDATED to: ' + datetime.utcfromtimestamp(self.ChargerEpoch).strftime('%d/%m/%Y %H:%M:%S'))
        
    def Get_Date_cmd(self):
        # Send 4 bytes key code
        with i2c.I2CMaster(self.bus) as master:
            app = self.read_ndata(master, Driver.SYSTEM_DATE_CMD, 4)
            self.ChargerEpoch = (app[0] << 24) + (app[1] << 16) + (app[2] << 8) + app[3]
        # print('N. of seconds since 01/Jan/1970 = {:d}'.format(self.Data))
        print('Charger UTC date ' + datetime.utcfromtimestamp(self.ChargerEpoch).strftime('%d/%m/%Y %H:%M:%S'))

    def Reboot_T(self, T1, T2):
        # Send 4 bytes key code
        if T1 < 0 or T1 > 255:
            print('{} is a wrong delay'.format(T1))
            return
        B0_T2 = np.uint8(T2)
        B1_T2 = np.uint8(T2 >> 8)
        B2_T2 = np.uint8(T2 >> 16)
        B3_T2 = np.uint8(T2 >> 24)
        #print('{:X} , {:X}, {:X}, {:X}'.format(B3_T2,B2_T2,B1_T2,B0_T2))
        with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_REB_T, np.uint8(T1), B3_T2, B2_T2, B1_T2, B0_T2))

    def Reboot_P(self, T1, P2, T2):
        # Send 4 bytes key code
        if T1 < 0 or T1 > 255:
            print('{} is a wrong delay'.format(T1))
            return
        if P2 < 0 or P2 > 100:
            print('{} is a wrong %'.format(P2))
        else:
            if T2 == 0:
                with i2c.I2CMaster(self.bus) as master:
                    master.transaction(i2c.writing_bytes(self.address, self.CMD_REB_P, np.uint8(T1), np.uint8(P2)))
            else:
                B0_T2 = np.uint8(T2)
                B1_T2 = np.uint8(T2 >> 8)
                B2_T2 = np.uint8(T2 >> 16)
                B3_T2 = np.uint8(T2 >> 24)
                with i2c.I2CMaster(self.bus) as master:
                    master.transaction(i2c.writing_bytes(self.address, self.CMD_REB_P, np.uint8(T1), np.uint8(P2), B3_T2, B2_T2, B1_T2, B0_T2))

    def Reboot_D(self, T1, D2):
        # Send 4 bytes key code
        if T1 < 0 or T1 > 255:
            print('{} is a wrong delay'.format(T1))
            return
        B0_D2 = np.uint8(D2)
        B1_D2 = np.uint8(D2 >> 8)
        B2_D2 = np.uint8(D2 >> 16)
        B3_D2 = np.uint8(D2 >> 24)
        #print('{:X} , {:X}, {:X}, {:X}'.format(B3_D2,B2_D2,B1_D2,B0_D2))
        with i2c.I2CMaster(self.bus) as master:
                master.transaction(i2c.writing_bytes(self.address, self.CMD_REB_D, np.uint8(T1), B3_D2, B2_D2, B1_D2, B0_D2))

    def read_Register(self, Register):
        # Send command to read
        lsb_Reg = np.uint8(Register)
        msb_Reg = np.uint8(Register >> 8)
        print ('lsb_Reg:', lsb_Reg)
        print ('msb_Reg:', msb_Reg)
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, msb_Reg, lsb_Reg))

        highByte, lowByte = master.transaction(i2c.reading(self.address, 2))[0]
        result = (highByte << 8) + lowByte

        return result

    def read_Charge_Status(self):
        '''
        Read the Charge Status  
        '''
        with i2c.I2CMaster(self.bus) as master:
            data = self.read_ndata(master, Driver.CMD_C_STATUS,10)
            self.Charge = (data[0] << 8) + data[1]
            self.Tension = (data[2] << 8) + data[3]
            self.Current = (data[4] << 8) + data[5]
            if self.Current > 0x7FFF: # two's complement
                self.Current -= 65536
            self.Temperature = (((data[6] << 8) + data[7])/100 - 273.15)
            self.codex = data[8]
            self.System = data[9]
            print('Charge = {} % \n'.format(self.Charge))
            print('Tension = {} mV \n'.format(self.Tension))
            print('Current = {} mA \n'.format(self.Current))
            print('Temperature = {:.2f} C \n'.format(self.Temperature))
            print('codex = {:X} \n'.format(self.codex))
            print('System = {:X} \n'.format(self.System))

    def read_BMS_Status(self, d):
        '''
        Read the BMS Status  
        '''
        if d > 6:
            print('wrong index')
            return
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_B_STATUS,np.uint8(d))
            print('{} \n'.format(res))

    def read_Gauge_Status(self, d):
        '''
        Read the Fuel Gauge Status  
        '''
        if d > 6:
            print('wrong index')
            return
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_G_STATUS,np.uint8(d))
            print('{} \n'.format(res))

    def Read_Charge(self):
        '''
        Read the Charge Status  
        '''
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_B_STATUS,0)
            print('Level of charge = {} %\n'.format(res))

    def Read_Tension(self):
        '''
        Read the Battery Voltage
        '''
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_B_STATUS,3)
            print('Tension of battery = {} mV\n'.format(res))

    def Read_Current(self):
        '''
        Read the Battery Current
        '''
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_G_STATUS,3)
            if res > 0x7FFF: # two's complement
                res -= 65536
            print('Current of battery = {} mA\n'.format(res))
            
    def Read_Alarm(self):
        '''
        Read the Charge Status  
        '''
        with i2c.I2CMaster(self.bus) as master:
            res = self.read_index(master, Driver.CMD_B_STATUS,1)
        print('Alarm World = {}'.format(res))
'''
        if res[15] == 1:
            print('WARNING: Minimum Tension in charge\n')
        if res[14] == 1:
            print('WARNING: Minimum Temperature\n')
        if res[13] == 1:
            print('WARNING: Minimum Level of Energy\n')
        if res[12] == 1:
            print('WARNING: Minimum Tension in discharge\n')
        if res[11] == 1:
            print('WARNING: Maximum Tension in charge\n')
        if res[10] == 1:
            print('WARNING: Maximum Temperature of Board\n')
        if res[9] == 1:
            print('WARNING: Maximum Temperature of Cell\n')
        if res[8] == 1:
            print('WARNING: Maximum Current\n')
        if res[7] == 1:
            print('ALARM: Minimum Tension in charge\n')
        if res[6] == 1:
            print('ALARM: Minimum Temperature\n')
        if res[5] == 1:
            print('ALARM: Minimum Level of Energy\n')
        if res[4] == 1:
            print('ALARM: Minimum Tension in discharge\n')
        if res[3] == 1:
            print('ALARM: Maximum Tension in charge\n')
        if res[2] == 1:
            print('ALARM: Maximum Temperature of Board\n')
        if res[1] == 1:
            print('ALARM: Maximum Temperature of Cell\n')
        if res[0] == 1:
            print('ALARM: Maximum Current\n')
'''
if __name__ == "__main__":
    bus = i2cutils.i2c_raspberry_pi_bus_number()
    #print ('RPi I2C Bus nbr: %s' % str(bus))
    #Driver = Driver(bus, Driver.I2C_ADDRESS_HIGH, Driver.PRECISION_ADC_4096,  Driver.PRECISION_ADC_4096)
    Driver = Driver(bus, Driver.I2C_ADDRESS_HIGH)

    print('Press ENTER for Command List\n')
    while True:
        #Driver.ms5803_read()
        s = input("Insert command:")
        if (s == '1'):
            Driver.read_Charge_Status()
        elif (s == '2'):
            d = int(input("Insert index:"))
            Driver.read_BMS_Status(d)
        elif (s == '3'):
            d = int(input("Insert index:"))
            Driver.read_Gauge_Status(d)
        elif (s == '4'):
            d = int(input("Insert Delay [sec]:"))
            Driver.Power_off_cmd(d)
        elif (s == '5'):
            d = int(input("Insert command:[1 = on, 0 = off]"))
            Driver.Power_Bat_cmd(d)
        elif (s == '6'):
            d = int(input("Insert command:[1 = on, 0 = off]"))
            Driver.Power_3V3_cmd(d)
        elif (s == '7'):
            d = int(input("Insert command:[1 = on, 0 = off]"))
            Driver.Power_12_cmd(d)
        elif (s == '8'):
            d = int(input("Insert command:[0 = all off, 1 = on Laser0, 2 = on Laser1, 3 = all on]"))
            Driver.Power_Laser_cmd(d)
        elif (s == '9'):
            d = int(input("Insert command:[1 = on, 0 = off]"))
            Driver.Power_5_cmd(d)
        elif (s == '10'):
            # d = int(input("Insert Data [sec]:"))
            Driver.Set_Date_cmd()
        elif (s == '11'):
            Driver.Get_Date_cmd()
        elif (s == '12'):
            d1 = int(input("Insert Delay of switch off [sec]:"))
            d2 = int(input("Insert Delay of switch on [sec]:"))
            Driver.Reboot_T(d1, d2)
        elif (s == '13'):
            d1 = int(input("Insert Delay of switch off [sec]:"))
            d2 = int(input("Insert % of switch on [%]:"))
            t2 = int(input("Insert Time-out Delay forcing switch-on [sec, 0 to skip]:"))
            Driver.Reboot_P(d1, d2, t2)
        elif (s == '14'):
            d1 = int(input("Insert Delay of switch off [sec]:"))
            Driver.Get_Date_cmd()
            dateinput = input("Insert UTC date of switch on [dd/mm/YYYY hh:mm:ss]:")
            d2 = int(datetime.strptime(dateinput, "%d/%m/%Y %H:%M:%S").timestamp())
            Driver.Reboot_D(d1, d2)
        elif (s == '15'):
            d = int(input("Insert Key:"))
            Driver.write_key32('Global', d)
        elif (s == '16'):
            d = int(input("Insert Key:"))
            Driver.write_key16('Global',d)
        elif (s == '17'):
            d = int(input("Insert Key:"))
            Driver.write_key32('Local',d)
        elif (s == '18'):
            d = int(input("Insert Key:"))
            Driver.write_key16('Local',d)
        elif (s == '19'):
            Driver.Read_Charge()
        elif (s == '20'):
            Driver.Read_Tension()
        elif (s == '21'):
            Driver.Read_Current()
        elif (s == '22'):
            Driver.Read_Alarm()
        else:
            print('Command List:')
            print('1 = Charge Status')
            print('2 = BMS Status')
            print('3 = Fuel Gauge Status')
            print('4 = System Shut-Down (no reboot) power-cycle needed to restart')
            print('5 = Switch on/off Battery Charger')
            print('6 = Switch on/off 3V3')
            print('7 = Switch on/off 12V')
            print('8 = Switch on/off Laser')
            print('9 = Power on/off 5V instantly')
            print('10 = Set Date')
            print('11 = Get Date')
            print('12 = Switch off and Reboot after n. seconds')
            print('13 = Switch off and Reboot at % of charge')
            print('14 = Switch off and Reboot at a date')
            print('15 = Write 32bit Global Key')
            print('16 = Write 16bit Global Key')
            print('17 = Write 32bit Local Key')
            print('18 = Write 16bit Local Key')
            print('19 = Read Charge [%]')
            print('20 = Read Tension [mV]')
            print('21 = Read Current [mA]')
            print('22 = Read Alarm\n')

        sleep(1)


class PowerDriver(Driver, AbstractPowerDriver):
    def __init__(self, i2c, gpio):
        super(PowerDriver, self).__init__(bus=i2c, address=self.I2C_ADDRESS_HIGH)
        self.gpio = gpio
        if options.noop:
            self.write_command = self.write_command_noop

    def set_battery_charge(self, on):
        if on:
            log.info("switch ON battery charge")
            self.Power_Bat_cmd(1)
        else:
            log.info("switch OFF battery charge")
            self.Power_Bat_cmd(0)

    def set_5v_power_rail(self, on):
        if on:
            log.info("switch ON 5V power rail")
            self.Power_5_cmd(1)
        else:
            log.info("switch OFF 5V power rail")
            self.Power_5_cmd(0)

    def set_12v_power_rail(self, on):
        if on:
            log.info("switch ON 12V power rail")
            self.Power_12_cmd(1)
        else:
            log.info("switch OFF 12V power rail")
            self.Power_12_cmd(0)

