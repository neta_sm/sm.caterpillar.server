import logging
from threading import Thread, Lock
from queue import Queue
from sm.caterpillar.utils.singleton import singleton
from time import sleep
from .gpio import GPIO
from ...server.options import option
log = logging.getLogger(__name__)


@singleton
class LEDDriver(Thread):
    LED_MASK = 0b111111111

    OFF = 0b00000000

    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        for bit, o in self.LEDS:
            GPIO.setup(o, GPIO.OUT)
        self.set(0x0)
        self._lock = Lock()
        self._queue = Queue(maxsize=1)
        self.start()

    def set(self, mask):
        if mask & ~self.LED_MASK:
            raise RuntimeError("unsupported ON led bitmask: 0x%08x" % mask)
        for bit, o in self.LEDS:
            on = 0x1 if (bit & mask) else 0x0
            log.debug("switch %s led 0x%04x" , "ON" if on else "OFF", bit)
            GPIO.output(o, not on)

    def run(self):
        while True:
            mask, pulses, delay_us, duration_us = self._queue.get()
            log.debug("emitting %d LED pulses with mask 0x%04x, duration of %dmS, delay of %dmS",
                      pulses, mask, duration_us, delay_us)
            try:
                self.set(0x0)
                for i in range(pulses):
                    sleep(delay_us / 1000000)
                    self.set(mask)
                    sleep(duration_us / 1000000)
                    self.set(0x0)
            finally:
                self.set(0x0)

    def pulse(self, mask, pulses, delay_us, duration_us):
        self._queue.put_nowait((mask, pulses, delay_us, duration_us))


    @property
    def LEDS(self):
        try:
            return self._LEDS
        except AttributeError:
            if option.board_revision == 1:
                self._LEDS = [
                    # led#       IO BCM pin
                    (0b00000001, 10), # verde
                    (0b00000010, 9),  # giallo
                    (0b00000100, 25), # bianco sott
                    (0b00001000, 8),  # arancio
                    (0b00010000, 7),  # bianco beefy (guida)
                ]
            elif option.board_revision == 1:
                self._LEDS = [
                    # led#       IO BCM pin
                    (0b00000001, 7),
                    (0b00000010, 8),
                    (0b00000100, 10),
                    (0b00001000, 25),
                    (0b00010000, 9),
                ]
            else:
                self._LEDS = [
                    # led#       IO BCM pin
                    (0b000000001, 7),
                    (0b000000010, 8),
                    (0b000000100, 10),
                    (0b000001000, 25),
                    (0b000010000, 9),
                    (0b000100000, 5),
                    (0b001000000, 6),
                    (0b010000000, 12),
                    (0b100000000, 13),
                ]
        return self._LEDS

