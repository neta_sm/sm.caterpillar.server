import logging
from datetime import timedelta
from fractions import Fraction
from threading import Lock

from copy import copy
from picamera import PiCamera
from picamera.exc import PiCameraClosed
from sm.caterpillar.utils.devnull import DiscardStream
from sm.caterpillar.utils.singleton import singleton
from tornado.gen import coroutine, TimeoutError, sleep
from tornado.ioloop import IOLoop
from tornado.locks import Event
from weakref import WeakSet

from .gpio import GPIO

log = logging.getLogger(__name__)

# pin function  = GPIO#
PinMuxSel       = 4
PinMux1En1      = 17
PinMux1En2      = 18
PinMux2En1      = 22
PinMux2En2      = 23

NOCAM=1 # only adds meaning

DEFAULT_VIDEO_FORMAT = "h264"
DEFAULT_IMAGE_FORMAT = "jpeg"
DEFAULT_LOOP_TIME = timedelta(seconds=1)
DEFAULT_FRAMERATE = 60
DEFAULT_RESOLUTION = (640, 480)
DEFAULT_ISO = 0
DEFAULT_SETTLE_TIME = 2
DEFAULT_SHUTTER_SPEED = Fraction(1, 1)
REASONABLE_AWG_GAIN = (Fraction(77, 64), Fraction(243, 128)) # determined by direct observation, don't trust this too much


class StillSequenceEntry:
    def get_stream(self):
        raise NotImplementedError

    def enter(self):
        pass

    def exit(self, exc_type, exc_val, exc_tb):
        pass


class FeedLoop:
    def __init__(self,
                 driver,
                 format=DEFAULT_VIDEO_FORMAT,
                 camera=None,
                 framerate=DEFAULT_FRAMERATE,
                 shutter_speed=DEFAULT_SHUTTER_SPEED,
                 resolution=DEFAULT_RESOLUTION,
                 auto_mode=True,
                 iso=DEFAULT_ISO,
                 settle_time=DEFAULT_SETTLE_TIME,
                 flip_horiz=False,
                 flip_vert=False,
        ):
        self.driver = driver
        self._stop_signaled = Event()
        self._settled = Event()
        self._streams = self.driver._streams
        self._io_loop = IOLoop.current()
        self._engaged = Lock()
        self._format = format

        max_shutter_us = int(Fraction(1000000, framerate))
        shutter_speed = int(max_shutter_us * shutter_speed)

        self._camera = PiCamera(resolution=resolution, framerate=framerate)
        self._camera.start_preview()
        self._camera.iso = iso
        self._camera.vflip = flip_vert
        self._camera.hflip = flip_horiz
        self._camera.shutter_speed = shutter_speed
        self.is_active_video = False

        if not auto_mode:
            @coroutine
            def settler():
                yield sleep(settle_time)
                try:
                    self._camera.exposure_mode = "off"
                    # g = self._camera.awb_gains
                    # self._camera.awb_mode = "off"
                    # self._camera.awb_gains = g
                    self._settled.set()
                except PiCameraClosed:
                    pass
        else:
            @coroutine
            def settler():
                yield sleep(settle_time)
                self._settled.set()
        # self._camera.awb_mode = "sun"
        # self._camera.exposure_mode = "off"
        self._camera.exposure_mode = "off"
        self._camera.awb_mode = "off"
        self._camera.awb_gains = REASONABLE_AWG_GAIN

        self._io_loop.add_callback(settler)

    def signal_stop(self):
        self._stop_signaled.set()
        self._settled.set()

    @coroutine
    def video(self):
        if not self._engaged.acquire(False):
            raise RuntimeError("camera hardware already busy")
        camera = self._camera
        try:
            try:
                camera.start_recording(self, format=self._format)
                self.is_active_video = True
                try:
                    while not self._stop_signaled.is_set():
                        camera.wait_recording(timeout=0)
                        try:
                            yield self._stop_signaled.wait(timeout=DEFAULT_LOOP_TIME)
                        except TimeoutError:
                            pass
                except:
                    log.exception("hard error during video in management loop. "
                                  "loop was killed, bailing out")
                    self.signal_stop()
            finally:
                self.is_active_video = False
                camera.stop_recording()
                log.debug("video feed has terminated")
                kill_list = set()
                for stream in self._streams:
                    try:
                        stream.close()
                    except:
                        log.exception("error delivering data to video stream %r", stream)
                        kill_list.add(stream)
                self._streams -= kill_list
        finally:
            try: camera.close()
            except: pass
            try: self.driver._feed_thread_cleanup()
            except: pass
            self._engaged.release()

    def set_caption(self, txt=None):
        if txt:
            self._camera.annotate_text = txt

    @coroutine
    def still_sequence(self, sequence, use_video_port=True):
        if not self._engaged.acquire(False):
            raise RuntimeError("camera hardware already busy")
        iter=sequence
        def seq_wrapper():
            for i in iter:
                yield DiscardStream()
                if i.enter():
                    try:
                        yield i.get_stream()
                    finally:
                        i.exit()
        yield self._settled.wait()
        try:
            self._camera.capture_sequence(seq_wrapper(), use_video_port=use_video_port,
                                          format=self._format,
                                          burst=not use_video_port)
        finally:
            try: self._camera.close()
            except: pass
            try: self.driver._feed_thread_cleanup()
            except: pass
            self._engaged.release()

    def write(self, s):
        log.debug("video feed produces %d bytes of data", len(s))
        kill_list = set()
        for stream in self._streams:
            try:
                stream.write(s)
            except:
                log.exception("error delivering data to video stream %r", stream)
                kill_list.add(stream)
        self._streams -= kill_list

    def flush(self):
        log.debug("video feed requests flush of data")
        kill_list = set()
        for stream in self._streams:
            try:
                stream.flush()
            except:
                log.exception("error delivering data to video stream %r", stream)
                kill_list.add(stream)
        self._streams -= kill_list

    def emit_key_frame(self):
        if self._camera:
            self._camera.request_key_frame()

    def set_flip_vert(self, v):
        self._camera.vflip = v

    def set_flip_horiz(self, v):
        self._camera.hflip = v

    def set_iso(self, v):
        self._camera.iso = v


@singleton
class CameraDriver:
    CAMERA_1 = 1
    CAMERA_2 = 2
    CAMERA_3 = 3
    CAMERA_4 = 4
    CAMERA_5 = 5

    CAMERAS = {
        CAMERA_1: (
            (PinMux1En1, 0      ),
            (PinMux1En2, 1      ),
            (PinMux2En1, NOCAM  ),
            (PinMux2En2, NOCAM  ),
            (PinMuxSel , 0      ),
        ),
        CAMERA_2: (
            (PinMux1En1, 0      ),
            (PinMux1En2, 1      ),
            (PinMux2En1, NOCAM  ),
            (PinMux2En2, NOCAM  ),
            (PinMuxSel , 1      ),
        ),
        CAMERA_3: (
            (PinMux1En1, 1      ),
            (PinMux1En2, 0      ),
            (PinMux2En1, NOCAM  ),
            (PinMux2En2, NOCAM  ),
            (PinMuxSel , 0      ),
        ),
        CAMERA_4: (
            (PinMux1En1, 1      ),
            (PinMux1En2, 0      ),
            (PinMux2En1, 0      ),
            (PinMux2En2, 1      ),
            (PinMuxSel , 1      ),
        ),
        CAMERA_5: (
            (PinMux1En1, 1      ),
            (PinMux1En2, 0      ),
            (PinMux2En1, 1      ),
            (PinMux2En2, 0      ),
            (PinMuxSel , 1      ),
        ),
    }

    def __init__(self):
        for i in PinMux1En1, PinMux1En2, PinMux2En1, PinMux2En2, PinMuxSel:
            GPIO.setup(i, GPIO.OUT)
        self._pin = GPIO.output
        self._feed_loop = None
        self._streams = WeakSet()
        self._io_loop = IOLoop.current()
        self._disengage = Event()
        self._disengage.set()

        self._selected_camera = None
        self._auto_mode = True
        self._flip_vert = False
        self._flip_horiz = False
        self._resolution = DEFAULT_RESOLUTION
        self._framerate = DEFAULT_FRAMERATE
        self._shutter_speed = DEFAULT_SHUTTER_SPEED
        self._iso = DEFAULT_ISO
        self._settle_time = DEFAULT_SETTLE_TIME
        self._caption = None

    def switch_to(self, cameraId):
        try:
            for pin_id, val in self.CAMERAS[cameraId]:
                self._pin(pin_id, val)
                self._selected_camera = cameraId
            log.debug("switch camera mux chain to camera %r", cameraId)
        except KeyError:
            raise RuntimeError("invalid camera id: %r (valid ids are: %r)" % (cameraId, list(self.CAMERAS)))

    def set_caption(self, txt=None):
        if self._feed_loop:
            self._feed_loop.set_caption(txt)

    def shoot_sequence(self,
            sequence,
            long_exposure=False,
            use_video_port=False,
            auto_mode=True,
            resolution=None,
            framerate=None,
            shutter_speed=None,
            iso=None,
            settle_time=None,
            format=None,
    ):
        self._disengage.clear()
        if self._feed_loop is not None:
            raise RuntimeError("video feed is already active. use stop_feed() first")

        params = {}

        if long_exposure:
            use_video_port=False
            auto_mode=False

        auto_mode = bool(auto_mode)
        if self._auto_mode != auto_mode:
            params.update(auto_mode=auto_mode)
            self._auto_mode = auto_mode

        if resolution and self._resolution != resolution:
            params.update(resolution=resolution)
            self._resolution = resolution

        if framerate and self._framerate != framerate:
            params.update(framerate=framerate)
            self._framerate = framerate

        if shutter_speed and self._shutter_speed != shutter_speed:
            params.update(shutter_speed=shutter_speed)
            self._shutter_speed = shutter_speed

        if iso is not None and self._iso != iso:
            params.update(iso=iso)
            self._iso = iso

        if settle_time and self._settle_time != settle_time:
            params.update(settle_time=settle_time)
            self._settle_time = settle_time

        pass_params = dict(
            format=format if format else DEFAULT_IMAGE_FORMAT,
            framerate=self._framerate,
            shutter_speed=self._shutter_speed,
            resolution=self._resolution,
            auto_mode=self._auto_mode,
            iso=self._iso,
            settle_time=self._settle_time,
        )
        params = copy(params)
        for k in ("auto_mode",
                  "resolution", "framerate", "shutter_speed", "iso", "settle_time"):
            v = params.pop(k, None)
            if v is not None:
                pass_params.update(camera=v)
        if params:
            raise RuntimeError("unsupported camera startup params: %s" % (", ".join(params)))

        self.switch_to(CameraDriver.CAMERA_1)
        self._feed_loop = FeedLoop(self, **pass_params)
        self._io_loop.add_callback(self._feed_loop.still_sequence,
                                   sequence=sequence, use_video_port=use_video_port)

    def set_video_feed(
            self,
            active,
            camera=None,
            flip_vert=False,
            flip_horiz=False,
            auto_mode=True,
            resolution=None,
            framerate=None,
            shutter_speed=None,
            iso=None,
            settle_time=None,
            ):

        if not active:
            self._io_loop.add_callback(self.stop_feed)
            return

        to_stop = False
        to_start = not self._feed_loop
        params = {}

        if camera and self._selected_camera != camera:
            params.update(camera=camera)
            self._selected_camera = camera

        flip_vert = bool(flip_vert)
        if self._flip_vert != flip_vert:
            params.update(flip_vert=flip_vert)
            self._flip_vert = flip_vert

        flip_horiz = bool(flip_horiz)
        if self._flip_horiz != flip_horiz:
            params.update(flip_horiz=flip_horiz)
            self._flip_horiz = flip_horiz

        auto_mode = bool(auto_mode)
        if self._auto_mode != auto_mode:
            to_stop = True
            params.update(auto_mode=auto_mode)
            self._auto_mode = auto_mode

        if resolution and self._resolution != resolution:
            to_stop = True
            params.update(resolution=resolution)
            self._resolution = resolution

        if framerate and self._framerate != framerate:
            to_stop = True
            params.update(framerate=framerate)
            self._framerate = framerate

        if shutter_speed and self._shutter_speed != shutter_speed:
            to_stop = True
            params.update(shutter_speed=shutter_speed)
            self._shutter_speed = shutter_speed

        if iso is not None and self._iso != iso:
            params.update(iso=iso)
            self._iso = iso

        if settle_time and self._settle_time != settle_time:
            to_stop = True
            params.update(settle_time=settle_time)
            self._settle_time = settle_time

        @coroutine
        def set_feed_wrap():
            if to_stop:
                yield self.stop_feed()
                self.start_feed_ll(params)
            elif to_start:
                self.start_feed_ll(params)
            else:
                self.set_feed_ll(params)

        self._io_loop.add_callback(set_feed_wrap)

    def start_feed_ll(self, params):
        self._disengage.clear()
        if self._feed_loop is not None:
            raise RuntimeError("video feed is already active. use stop_feed() first")
        pass_params = dict(
            format=DEFAULT_VIDEO_FORMAT,
            camera=self._selected_camera,
            framerate=self._framerate,
            shutter_speed=self._shutter_speed,
            resolution=self._resolution,
            auto_mode=self._auto_mode,
            iso=self._iso,
            settle_time=self._settle_time,
            flip_horiz=self._flip_horiz,
            flip_vert=self._flip_vert,
        )
        params = copy(params)
        for k in ("camera", "flip_vert", "flip_horiz", "auto_mode",
                  "resolution", "framerate", "shutter_speed", "iso", "settle_time"):
            v = params.pop(k, None)
            if v is not None:
                pass_params.update(camera=v)
        if params:
            raise RuntimeError("unsupported camera startup params: %s" % (", ".join(params)))
        self.switch_to(self._selected_camera)
        self._feed_loop = FeedLoop(self, **pass_params)
        self._io_loop.add_callback(self._feed_loop.video)

    def set_feed_ll(self, params):
        if self._feed_loop is None:
            raise RuntimeError("video feed is not active. use start_feed_ll() first")
        params = copy(params)
        v = params.pop("camera", None)
        if v is not None:
            self.switch_to(v)
        v = params.pop("flip_vert", None)
        if v is not None:
            self._feed_loop.set_flip_vert(v)
        v = params.pop("flip_horiz", None)
        if v is not None:
            self._feed_loop.set_flip_horiz(v)
        v = params.pop("iso", None)
        if v is not None:
            self._feed_loop.set_iso(v)
        if params:
            raise RuntimeError("unsupported camera override params: %s" % (", ".join(params)))

    # def start_feed(self,
    #                format=DEFAULT_VIDEO_FORMAT,
    #                framerate=DEFAULT_FRAMERATE,
    #                shutter_speed=DEFAULT_SHUTTER_SPEED,
    #                resolution=DEFAULT_RESOLUTION,
    #                auto_mode=True,
    #                iso=DEFAULT_ISO,
    #                settle_time=DEFAULT_SETTLE_TIME
    #                ):
    #     self._disengage.clear()
    #
    #     if self._feed_loop is not None:
    #         return
    #
    #     if settle_time is DEFAULT_SETTLE_TIME:
    #         settle_time = framerate * 5
    #     max_shutter_us = int(Fraction(1000000, framerate))
    #     shutter_speed = int(max_shutter_us * shutter_speed)
    #
    #     self._feed_loop = FeedLoop(self,
    #                                format=format,
    #                                framerate=framerate,
    #                                resolution=resolution,
    #                                auto_mode=auto_mode,
    #                                iso=iso,
    #                                settle_time=settle_time,
    #                                shutter_speed=shutter_speed)
    #     self._io_loop.add_callback(self._feed_loop)

    @coroutine
    def stop_feed(self):
        if self._feed_loop is not None:
            self._feed_loop.signal_stop()
        yield self._disengage.wait()

    def _feed_thread_cleanup(self):
        self._feed_loop = None
        self._disengage.set()

    def tap_feed_data(self, fileobj):
        self._streams.add(fileobj)
        if self._feed_loop:
            self._feed_loop.emit_key_frame()

    @property
    def is_active_video(self):
        return self._feed_loop and self._feed_loop.is_active_video

    @property
    def disengage(self):
        return self._disengage

"""



# -*- coding: utf-8 -*-

#per documentatazione  ref. API PiCamera:
#https://picamera.readthedocs.io/en/release-1.10/

#CamView ver. 1.0.8
#rispetto la versione precedente:
#customizzata procedura capture_sequence(...)

#Libraries
#import shutil
#import threading
import sys
#import RPi.GPIO as GPIO
from .gpio import GPIO
from sm.caterpillar.utils.singleton import singleton


try:
    #print((len(sys.argv)))
    #for temp in sys.argv:
        #print(('camera: ' + temp))

    if len(sys.argv) > 1:
        if sys.argv[1] != '':
            if sys.argv[1] == '-h' or sys.argv[1] == '--help':
                print('\nCameraView parameter list:')
                print('\tusing: python CameraView.py numCam useMux\n')
                print('numCam => number of installed cameras (1-4), \n\t  0 for using default 1 camera without mux')
                print('useMux => (y | n) if you have 2 or more installed cameras setted default as y.\n\t  Useful if you use only 1 camera on mux')
                print('\nNo parameter the default values is setted to 4 cameras and use mux\n')
            else:
                camNum = int(sys.argv[1])

        if len(sys.argv) > 2:
            if sys.argv[2] != '' and camNum < 2:
                if sys.argv[2] == 'y' or sys.argv[2] == 'Y':
                    useMux = True
                if sys.argv[2] == 'n' or sys.argv[2] == 'N':
                    useMux = False
        else:
            useMux = True
except:
    print('\nCameraView error: wrong parameter')
    print('use \'-h\' or \'--help\' for parameter list')
    sys.exit(2)

if len(sys.argv) > 1:
    if sys.argv[1] == '-h' or sys.argv[1] == '--help':
        sys.exit(0)

@singleton
class CameraDriver:
    LED1 = 10
    LED2 = 9
    LED3 = 25
    LED4 = 8
    LED5 = 7  # guida
    M2PinEn1 = 22
    M2PinEn2 = 23
    M1PinEn1 = 17
    M1PinEn2 = 18
    SelPin = 4

    CAMERAS = {
            #M1PinEn1, M1PinEn2, SelPin
        1: [0,         1,        0],
        2: [0,         1,        0],
        3: [0,         1,        0],
        4: [0,         1,        0],
    }

    def __init__(self):
        GPIO.setup(self.M1PinEn1, GPIO.OUT)
        GPIO.setup(self.M1PinEn2, GPIO.OUT)
        GPIO.setup(self.M2PinEn1, GPIO.OUT)
        GPIO.setup(self.M2PinEn2, GPIO.OUT)
        GPIO.setup(self.LED1, GPIO.OUT)
        GPIO.setup(self.LED2, GPIO.OUT)
        GPIO.setup(self.LED3, GPIO.OUT)
        GPIO.setup(self.LED4, GPIO.OUT)
        GPIO.setup(self.LED5, GPIO.OUT)
        GPIO.setup(self.SelPin, GPIO.OUT)
        GPIO.output(self.LED1, 1)
        GPIO.output(self.LED2, 1)
        GPIO.output(self.LED3, 1)
        GPIO.output(self.LED4, 1)
        GPIO.output(self.LED5, 1)
        GPIO.output(self.SelPin, 0)
        GPIO.output(self.M1PinEn1, 1)
        GPIO.output(self.M1PinEn2, 1)
        GPIO.output(self.M2PinEn1, 1)
        GPIO.output(self.M2PinEn2, 1)

    def enable_camera(self, n):
        flags = self.CAMERAS.get(n)
        if flags is None:
            raise RuntimeError("unsupported camera id: %r" % n)
        M1PinEn1, M1PinEn2, SelPin = flags
        GPIO.output(self.M1PinEn1, M1PinEn1)
        GPIO.output(self.M1PinEn2, M1PinEn2)
        GPIO.output(self.SelPin  , SelPin  )

if __name__ == '__main__':
    camera = CameraDriver.instance()
    from time import sleep
    sleep(1)
    camera.enable_camera(1)

#
# import datetime
# import time
# import picamera
# import picamera.mmal as mmal
# from picamera.exc import (PiCameraRuntimeError, mmal_check)
#
# source = '/media/cam_tmp/'
# dest = '/home/pi/share/'
# compression = 'jpeg'
# dateFormat = '%Y%m%d_%H%M%S.%f'
# extension = '.jpg'
# camNum = 2
# useMux = True
# vidPort = False
#
# def MyCapture_sequence(camera, outputs, formType = 'jpeg', use_video_port = False,
# resize = None, splitter_port = 0, burst = False, TotalCam = 4, **options):
#
#         if use_video_port and burst:
#             raise PiCameraRuntimeError(
#                 'Burst is only valid with still port captures')
#         with camera._encoders_lock:
#             camera_port, output_port = camera._get_ports(
#                 use_video_port, splitter_port)
#             formType = camera._get_image_format('', formType)
#             if formType == 'jpeg' and not use_video_port and not resize:
#                 camera._still_encoding = mmal.MMAL_ENCODING_OPAQUE
#             else:
#                 camera._still_encoding = mmal.MMAL_ENCODING_I420
#             if use_video_port:
#                 encoder = camera._get_images_encoder(
#                         camera_port, output_port, formType, resize, **options)
#                 camera._encoders[splitter_port] = encoder
#             else:
#                 encoder = camera._get_image_encoder(
#                         camera_port, output_port, formType, resize, **options)
#         try:
#             if use_video_port:
#                 encoder.start(outputs)
#                 encoder.wait()
#             else:
#                 if burst:
#                     mmal_check(
#                         mmal.mmal_port_parameter_set_boolean(
#                             camera_port,
#                             mmal.MMAL_PARAMETER_CAMERA_BURST_CAPTURE,
#                             mmal.MMAL_TRUE),
#                         prefix="Failed to set burst capture")
#
#                 try:
#                     if TotalCam > 0:
#                         if useMux:
#                             x = 1
#                         else:
#                             x = 0
#                     else:
#                         x = TotalCam = 0
#
#                     for i in range(x, TotalCam + 1):
#                         filename = CameraChange(i)
#                         encoder.start(filename)
#                         if not encoder.wait():
#                             #camera.CAPTURE_TIMEOUT):
#                             raise PiCameraRuntimeError(
#                             'Timed out waiting for capture to end')
#                 finally:
#                     if burst:
#                         mmal_check(
#                             mmal.mmal_port_parameter_set_boolean(
#                                 camera_port,
#                                 mmal.MMAL_PARAMETER_CAMERA_BURST_CAPTURE,
#                                 mmal.MMAL_FALSE),
#                             prefix="Failed to set burst capture")
#         finally:
#             encoder.close()
#             with camera._encoders_lock:
#                 if use_video_port:
#                     del camera._encoders[splitter_port]
#
#
# #--------------------------------------
# #metodo che effettua la foto settando la giusta posizione del mux,
# #param: num = numero camera del mux, = 0 se non è usato il mux
# def CameraShot(num):
#     #print('take photo from camera: ' + str(num))
#     #Accensione LEd
#     Led(num, 1)
#     camS.capture(CameraChange(num), format=compression, use_video_port=vidPort)
#     #foto = CameraChange(num)
#     #print('foto path: ' + foto)
#     #camS.capture_sequence(foto, format=compression, use_video_port=False, resize=None, splitter_port=0, burst=False)
#     Led(num, 0) #spegnimento led
#
# def Led(num, sw):
#     #print('take photo from camera: ' + str(num))
#     #Accensione LEd
#     if num == 0:
#         #all off
#         GPIO.output(LED1, 1)
#         GPIO.output(LED2, 1)
#         GPIO.output(LED3, 1)
#         GPIO.output(LED4, 1)
#     elif num == 1:
#         if sw == 1:
#             GPIO.output(LED1, 0)
#         else:
#             GPIO.output(LED1, 1)
#     elif num == 2:
#         if sw == 1:
#             GPIO.output(LED2, 0)
#         else:
#             GPIO.output(LED2, 1)
#     elif num == 3:
#         if sw == 1:
#             GPIO.output(LED3, 0)
#         else:
#             GPIO.output(LED3, 1)
#     elif num == 4:
#         if sw == 1:
#             GPIO.output(LED4, 0)
#         else:
#             GPIO.output(LED4, 1)
#     else:
#         #all off
#         GPIO.output(LED1, 1)
#         GPIO.output(LED2, 1)
#         GPIO.output(LED3, 1)
#         GPIO.output(LED4, 1)
#
# #--------------------------------------
#
# #inizializzo l'oggetto 'camera'
# camS = picamera.PiCamera()
# #vari settaggi del sensore quali: sensibilità iso, velocità otturatore, metodo di esposizione ecc...
# #camS.resolution = (2592, 1944)
# camS.resolution = (1920, 1080)
# #imposto velocità dell'otturatore a 1/250 (4000usec)
# camS.shutter_speed = 40000
# camS.image_denoise = False
# camS.image_effect = 'none'
# camS.awb_mode = 'auto'
# camS.iso = 800
# camS.meter_mode = 'backlit'
# camS.annotate_text = ''
# camS.drc_strength = 'off'
# camS.exposure_mode = 'off'
# camS.flash_mode = 'off'
# camS.image_denoise = 'off'
#
# curr_time = lambda: datetime.datetime.now().strftime(dateFormat)[:-3]
#
# #--------------------------------------
#
# try:
#     #Raise an exception if the camera is already closed
#     camS._check_camera_open()
#     #avvio il sensore della telecamera
#     camS.start_preview()
#     #Camera warm-up time
#     time.sleep(0.1)
#     #--------------------------------------
#
#     if camNum > 0:
#         if useMux:
#             x = 1
#         else:
#             x = 0
#     else:
#         x = camNum = 0
#
#     #print(('x: ' + str(x) + ' - camNum: ' + str(camNum)))
#     for i in range(x, camNum + 1):
#         CameraShot(i)
#
#     #MyCapture_sequence(camS, 'pippo1', formType=compression, use_video_port=False,
#     #resize=None, splitter_port=0, burst=False, TotalCam=camNum)
#
# finally:
#     #chiudo le camere prima di uscire
#     camS.stop_preview()
#     camS.close()
#     #attesa che il thread principale finisca effettivamente la chiusura delle camere&CO.
#     time.sleep(1)
#
#
#
#
#

"""
