import logging

import numpy as np
from tornado.options import options

from sm.caterpillar.utils.i2c import i2c
from .gpio import GPIO

log = logging.getLogger(__name__)

SW12 = 27 # Switch 12V

GPIO.setup(SW12, GPIO.OUT)
GPIO.output(SW12, 0)


class Driver(object):
    '''
    Simple Driver implementation for I2C communication (pin6 PS high)
    '''
    I2C_ADDRESS_HIGH = 0x71 # Pin1 is high (default)

    CMD_5ON     = 0x81     # Power ON 5V Command
    CMD_5OFF    = 0x80     # Power OFF 5V Command
    CMD_BC_ON   = 0x41     # Power ON Battery Charge
    CMD_BC_OFF  = 0x40     # Power OFF Battery Charge
    CMD_POFF_T  = 0x82     # Power OFF/ON 5V after T*3min
    CMD_POFF_C  = 0x84     # Power OFF/ON 5V after %C
    CMD_G_KEY   = 0x21     # Write Global Key
    CMD_L_KEY   = 0x22     # Write Local Key

    READ_V      = 0x11     # Read Tension Command
    READ_A      = 0x12     # Read Current Command
    READ_T      = 0x14     # Read Temperature Command
    READ_C_100  = 0x15     # Read Gauge % Command
    READ_C_C    = 0x16     # Read Gauge Coulomb Command

    OFFSET_CURR = 32767

    def __init__(self, bus, address):
        '''
        Constructor
        '''
        self.bus = bus           # I2C bus interface 0|1 [/dev/i2c-0 /dev/i2c-1]
        self.address = address   # I2C address

        self.Global_key32 = [0xAA, 0x11, 0xBB, 0x00]
        self.Global_key16 = [0xDD, 0x33]
        self.Local_key32 = [0xBB, 0x22, 0xCC, 0x11]
        self.Local_key16 = [0xEE, 0x44]


        self.Temperature = 0
        self.Tension = 0
        self.Current = 0
        self.Charge_100 = 0
        self.Charge_Cou = 0

        # reset the sensor
        # bus.write_byte(self.address, CMD_RESET)

    def read_data(self, master, cmd_measure):
        # Send command to read
        master.transaction(i2c.writing_bytes(self.address, cmd_measure))
        # Wait for conversion to complete
        #time.sleep(0.001);

        # Request read command
        #master.transaction(i2c.writing_bytes(self.address, Driver.CMD_ADC_READ))
        highByte, lowByte = master.transaction(i2c.reading(self.address, 2))[0]
        result = (highByte << 8) + lowByte

        return result
    def write_command(self, cmd):
        # Send command to write
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, cmd))
    def write_cmd_data(self, cmd, data):
        # Send command + data to write
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, cmd, data))
    def write_key32(self, cmd):
        # Send 2 bytes key code
        #print ('msb: ', self.key[0])
        #print ('lsb: ', self.key[1])
        with i2c.I2CMaster(self.bus) as master:
            if (cmd == 'Local'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_L_KEY, self.Local_key32[0], self.Local_key32[1], self.Local_key32[2], self.Local_key32[3]))
            elif (cmd == 'Global'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_G_KEY, self.Global_key16[0], self.Global_key16[1], self.Global_key16[2], self.Global_key16[3]))
            else:
                print ('ERROR: Command wrong:')

    def write_key16(self, cmd):
        # Send 2 bytes key code

        with i2c.I2CMaster(self.bus) as master:
            if (cmd == 'Local'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_L_KEY, self.Local_key16[0], self.Local_key16[1]))
            elif (cmd == 'Global'):
                master.transaction(i2c.writing_bytes(self.address, self.CMD_G_KEY, self.Global_key16[0], self.Global_key16[1]))
            else:
                print ('ERROR: Command wrong:')

    def read_Register(self, Register):
        # Send command to read
        lsb_Reg = np.uint8(Register)
        msb_Reg = np.uint8(Register >> 8)
        print ('lsb_Reg:', lsb_Reg)
        print ('msb_Reg:', msb_Reg)
        with i2c.I2CMaster(self.bus) as master:
            master.transaction(i2c.writing_bytes(self.address, msb_Reg, lsb_Reg))

        highByte, lowByte = master.transaction(i2c.reading(self.address, 2))[0]
        result = (highByte << 8) + lowByte

        return result

    def read_V(self):
        '''
        Read the Tension in mV
        '''
        with i2c.I2CMaster(self.bus) as master:
            self.Tension = self.read_data(master, Driver.READ_V)
        return self.Tension

    def read_A(self):
        '''
        Read the Amp in mA
        '''
        with i2c.I2CMaster(self.bus) as master:
            self.Current = self.read_data(master, Driver.READ_A)
        if (self.Current > self.OFFSET_CURR):
            self.Current = self.Current - self.OFFSET_CURR
            print("Battery in Charge")
        else:
            self.Current = self.OFFSET_CURR - self.Current
            print("Battery in Discharge")
        return self.Current

    def read_T(self):
        '''
        Read the Temperature in Celsius
        '''
        with i2c.I2CMaster(self.bus) as master:
            self.Temperature = (self.read_data(master, Driver.READ_T) - 273)
        return self.Temperature

    def read_Charge_100(self):
        '''
        Read the Charge in %
        '''
        with i2c.I2CMaster(self.bus) as master:
            self.Charge_100 = self.read_data(master, Driver.READ_C_100)
        return self.Charge_100

    def read_Charge_C(self):
        '''
        Read the Charge in Coulomb
        '''
        with i2c.I2CMaster(self.bus) as master:
            self.Charge_Cou = self.read_data(master, Driver.READ_C_C)
        return self.Charge_Cou


class PowerDriver(Driver):
    def __init__(self, i2c, gpio):
        super(PowerDriver, self).__init__(bus=i2c, address=self.I2C_ADDRESS_HIGH)
        self.gpio = gpio
        if options.noop:
            self.write_command = self.write_command_noop

    def set_battery_charge(self, on):
        if on:
            log.info("switch ON battery charge")
            self.write_command(self.CMD_BC_ON)
        else:
            log.info("switch OFF battery charge")
            self.write_command(self.CMD_BC_OFF)

    def set_5v_power_rail(self, on):
        if on:
            log.info("switch ON 5V power rail")
            self.write_command(Driver.CMD_5ON)
        else:
            log.info("switch OFF 5V power rail")
            self.write_command(Driver.CMD_5OFF)

    def set_12v_power_rail(self, on):
        if on:
            log.info("switch ON 12V power rail")
            self.gpio.output(SW12, 1)
        else:
            log.info("switch OFF 12V power rail")
            self.gpio.output(SW12, 0)
