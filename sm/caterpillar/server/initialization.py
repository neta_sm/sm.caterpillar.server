import logging

from sm.caterpillar.server.control.camera import CameraDriver
from sm.caterpillar.server.control.driver_es import Modbus
from sm.caterpillar.utils.i2c.i2cutils import i2c_raspberry_pi_bus_number
from sm.caterpillar.utils.singleton import singleton
from .control.gpio import GPIO
from .control.leds import LEDDriver
from .options import option
log = logging.getLogger(__name__)



@singleton
class Control:
    def __init__(self):
        if option.board_revision == 1:
            log.warning("using UNSUPPORTED board revision 1")
            from .control.power import PowerDriver
        elif option.board_revision == 2:
            log.info("using board revision 2")
            from .control.powerv2 import PowerDriver
        else:
            raise RuntimeError("unsupported board revision %r", option.board_revision)

        self.i2c = i2c_raspberry_pi_bus_number()
        log.debug('RPi I2C Bus nbr: %s', str(self.i2c))
        self.gpio = GPIO
        self.power = PowerDriver(self.i2c, self.gpio)
        self.modbus = Modbus.instance()
        self.leds = LEDDriver.instance()
        self.camera = CameraDriver.instance()


