from tornado.ioloop import IOLoop


class StatusDispatcher:
    def __init__(self):
        self._io_loop = IOLoop.current()
        self._cbacks = set()
        self._registry.add(self)

    def watched_by(self, cback):
        self._cbacks.add(cback)

    def _notify_status_change(self):
        for c in self._cbacks:
            self._io_loop.add_callback(c, self)

    _registry = set()

    @classmethod
    def broadcast_all(cls):
        for i in cls._registry:
            i._notify_status_change()

