from tornado.ioloop import IOLoop
from tornado.iostream import StreamClosedError
from tornado.locks import Event
from tornado.web import RequestHandler, Application, HTTPError, asynchronous
from tornado.gen import coroutine
from tornado.options import define, options
from os import environ
import logging
log = logging.getLogger(__name__)

DEFAULT_HTTP_CAMERA_PORT = int(environ.get("CAMERA_API_HTTP_PORT", 3136))
define("camera_api_http_port", type=int, help="TCP port for camera HTTP interaction", default=DEFAULT_HTTP_CAMERA_PORT)

AVERAGE_STREAM_CHUNK_LENGTH=1400
MAX_STREAM_BACKLOG_SIZE=1024*1024*5 # 5MB max backlog (after that, data is discarded, connection dropped)


class HTTPStreamRequestHandler(RequestHandler):
    def initialize(self, server):
        self.server = server
        self.byte_ctr = 0
        self.flushing = False
        self.flush_requested = False
        self.io_loop = IOLoop.current()
        self.over = Event()
        self.over_exception = None

    @property
    def camera(self):
        return self.server.machine.camera

    @asynchronous
    def get(self):
        if not self.camera.is_active_video:
            raise HTTPError(status_code=410, log_message="video stream not currently available")
        self.set_header("Content-Type", "video/H264")
        self.flush()
        self.camera.tap_feed_data(self)

    def write(self, chunk):
        if self.over.is_set():
            raise StreamClosedError
        RequestHandler.write(self, chunk)
        self.byte_ctr += len(chunk)
        if self.byte_ctr > MAX_STREAM_BACKLOG_SIZE:
            self.over_exception = HTTPError(status_code=408, log_message="excessive data buildup. laggy connection??")
            self.over.set()
        if self.byte_ctr > AVERAGE_STREAM_CHUNK_LENGTH:
            self.flush()

    def flush(self, *a, **ka):
        self.flush_requested = True
        if self.flushing:
            return
        self.io_loop.spawn_callback(self._flush_data, *a, **ka)

    @coroutine
    def _flush_data(self, *a, **ka):
        self.flushing = True
        try:
            while self.flush_requested:
                self.flush_requested = False
                self.byte_ctr = 0
                try:
                    yield RequestHandler.flush(self, *a, **ka)
                except GeneratorExit:
                    pass
                except:
                    log.exception("error while delivering data block")
                    self.close()
                    return
        finally:
            self.flushing = False

    def close(self):
        self.over.set()
        try:
            self.finish()
        except RuntimeError:
            # happens if called twice
            pass
        if self.over_exception:
            over_exception = self.over_exception
            self.over_exception = None
            raise over_exception



class HTTPStillRetrieve(RequestHandler):
    def initialize(self, server, still_id):
        self.server = server
        self.still_id = still_id

    @coroutine
    def get(self):
        data = self.server.stills.get(self.still_id)
        if not data:
            raise HTTPError(404)
        self.set_header("Content-Type", "image/jpeg")
        self.write(data)



def make_http_application(server):
    res = Application([
        (r"/cam/feed[\/]{0,1}", HTTPStreamRequestHandler, dict(server=server)),
        (r"/cam/still/1[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=1)),
        (r"/cam/still/2[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=2)),
        (r"/cam/still/3[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=3)),
        (r"/cam/still/4[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=4)),
        (r"/cam/still/5[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=5)),
        (r"/cam/still/6[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=6)),
        (r"/cam/still/7[\/]{0,1}", HTTPStillRetrieve, dict(server=server, still_id=7)),
    ])
    res.listen(options.camera_api_http_port)
    return res
