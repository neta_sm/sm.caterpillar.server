import logging
from queue import Queue, Full, Empty
from threading import Thread, Lock

log = logging.getLogger(__name__)

DEFAULT_QUEUE_LEN = 64

class RunQueue(Thread):
    def __init__(self, size=20):
        super(RunQueue, self).__init__()
        self.setDaemon(True)
        self._queue = Queue(size)
        self._over = False
        self._idle = True
        self._mutex = Lock()
        self.on_idle = []

    def run(self):
        while not self._over:
            try:
                if self._idle:
                    future, ftor = self._queue.get(timeout=1)
                    log.debug("unqueue (poll) %r", ftor)
                else:
                    future, ftor = self._queue.get_nowait()
                    log.debug("unqueue (immediate) %r", ftor)
                with self._mutex:
                    self._idle = False
            except Empty:
                with self._mutex:
                    self._idle = True
                    for cback in self.on_idle:
                        try:
                            cback()
                        except:
                            log.exception("error while dispatching idle-state commands [ignored]")
                    self.on_idle = []
                continue
            try:
                res = ftor()
                log.debug("%r -> %r", ftor, res)
                if future is not None:
                    future.set_result(res)
            except Exception as err:
                log.exception("error while executing async action %r [ignored]", ftor)
                if future is not None:
                    future.set_exception(err)

    def enqueue(self, ftor, future=None):
        try:
            self._queue.put_nowait((future, ftor))
        except Full as err:
            if future is not None:
                future.set_exception(err)
            else:
                raise

    def run_immediately_if_idle(self, ftor, future=None):
        if self.idle:
            self._queue.put_nowait((future, ftor))
        else:
            raise Full

    @property
    def idle(self):
        with self._mutex:
            return self._idle

