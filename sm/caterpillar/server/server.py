import logging

from os import environ, listdir
from os.path import isdir, join
from sm.caterpillar.udp import UDPServer
from sm.caterpillar.utils.singleton import singleton
from tornado.concurrent import TracebackFuture
from tornado.gen import coroutine
from tornado.ioloop import IOLoop
from tornado.options import options, define, parse_config_file

from .commands import Command
from .http_streaming import make_http_application
from .initialization import Control
from .machine import MachineStatus
from .run_queue import RunQueue

log = logging.getLogger(__name__)

define("bind_host", default="0.0.0.0", type=str)
define("bind_port", default=3135, type=int)
define("config", type=str, metavar="FILE", help="optional config file",
       callback=lambda x: parse_config_file(x, final=False))
STILLS_SAVE_DIR = environ.get("STILLS_SAVE_DIR", "")
define("stills_save_dir", default=STILLS_SAVE_DIR, help="stills save directory (default: disabled)")
VIDEO_SAVE_DIR = environ.get("VIDEO_SAVE_DIR", "")
define("video_save_dir", default=VIDEO_SAVE_DIR, help="video save directory")

RAW_NEG_OFFSET = -6404096

@singleton
class CameraFeed:
    OFF = object()

    def __init__(self, server):
        self._leds = server.machine.leds
        self._camera = server.machine.camera
        self._current = self.OFF

    def set(self, cameraId):
        if cameraId == self._current:
            return
        leds = self._leds
        camera = self._camera
        if cameraId == self.OFF:
            leds.set(leds.OFF)
            camera.stop_feed()
            return
        camera.stop_feed()
        camera.switch_to(cameraId)
        camera.start_feed()


class Server:
    def __init__(self):
        self.control = Control.instance()
        self.udp_server = UDPServer((options.bind_host, options.bind_port))
        self.over = False
        self.io_loop = IOLoop.current()
        self.machine = MachineStatus()
        self.http_iface = make_http_application(self)
        self.stills = {}
        self.still_seq_no = self.get_last_still_no()
        self.video_save_queue = RunQueue()
        self.video_save_fd = None
        self.video_save_delegate = None
        self.video_save_queue.start()

    @coroutine
    def serve_loop(self):
        while not self.over:
            try:
                data, source = yield self.udp_server.read_chunk()
                log.debug("got %d bytes of data from %r: %r", len(data), source, data)
                cmd = Command.fromData(data)
                log.debug("received command %r", cmd)
                cmd.source = source # TODO, use a better way to propagate this
                response = yield cmd(self)
                rdata = response.toData()
                log.debug("sending %s bytes of data to %r: %r", len(rdata), source, rdata)
                self.udp_server.sendto(rdata, source)
            except:
                log.exception("unhandler exception during serve loop [ignored]")

    def clear_stills(self):
        self.stills.clear()

    @staticmethod
    def get_last_still_no():
        _max = 0
        if not isdir(options.stills_save_dir):
            return 0
        for fn in listdir(options.stills_save_dir):
            _max = max(Server.still_seq_no(fn), _max)
        return _max

    @staticmethod
    def get_last_video_no():
        _max = 0
        if not isdir(options.video_save_dir):
            return 0
        for fn in listdir(options.video_save_dir):
            _max = max(Server.video_seq_no(fn), _max)
        return _max

    @staticmethod
    def still_name(seq, n, ext):
        return "caterpillar_%04d_cam%d%s" % (seq, n, ext)

    @staticmethod
    def video_name(seq):
        return "video_%04d%s" % (seq, ".h264")

    @staticmethod
    def still_seq_no(fn):
        if not fn:
            return 0
        ofs = fn.find("caterpillar_")
        if ofs != 0:
            return 0
        if len(fn) < 17:
            return 0
        ns = fn[12:16]
        try:
            return int(ns)
        except (ValueError, TypeError):
            return 0

    @staticmethod
    def video_seq_no(fn):
        if not fn:
            return 0
        ofs = fn.find("video_")
        if ofs != 0:
            return 0
        if len(fn) < 11:
            return 0
        ns = fn[6:10]
        try:
            return int(ns)
        except (ValueError, TypeError):
            return 0

    def save_stills(self, ext=".jpg"):
        self.still_seq_no += 1
        if not isdir(options.stills_save_dir):
            return
        for k, v in self.stills.items():
            fn = join(options.stills_save_dir, self.still_name(self.still_seq_no, k, ext))
            log.info("photo capture %04d: saving camera %d still picture to %s", self.still_seq_no, k, fn)
            try:
                with open(fn, "wb") as fd:
                    fd.write(v)
            except:
                log.exception("error while saving data to %s", fn)

    def start_video_recording(self):
        if self.video_save_fd is not None:
            log.debug("already saving a video feed. request ignored")
            return self.video_save_delegate
        if not isdir(options.video_save_dir):
            log.error("unable to save video to directory: %s", options.video_save_dir)
            return

        fn = join(options.video_save_dir, self.video_name(self.get_last_video_no()+1))
        log.info("saving video feed to %s", fn)
        self.video_save_fd = open(fn, "wb")
        self.video_save_delegate = VideoFeedSaveDelegate(self)
        return self.video_save_delegate

    def stop_video_recording(self):
        if self.video_save_fd is None:
            log.debug("not currently recording a video feed. request ignored")
            return
        fd = self.video_save_fd
        def close_ftor():
            fd.flush()
            fd.close()
        log.info("saving of video feed was stopped")
        self.video_save_queue.enqueue(close_ftor)
        self.video_save_fd = None
        self.video_save_delegate = None

    def is_video_recording(self):
        return self.video_save_fd is not None


class VideoFeedSaveDelegate(object):
    def __init__(self, server):
        self.server = server

    def write(self, x):
        server = self.server
        if server.video_save_fd is not None:
            future = TracebackFuture()
            future.add_done_callback(self.on_feedback)
            def do_write():
                server.video_save_fd.write(x)
            server.video_save_queue.enqueue(do_write)

    def flush(self):
        server = self.server
        if server.video_save_fd is not None:
            future = TracebackFuture()
            future.add_done_callback(self.on_feedback)
            def do_flush():
                server.video_save_fd.flush()
            server.video_save_queue.enqueue(do_flush)

    def close(self):
        log.info("video feed save stopped because the camera feed was closed")
        self.server.stop_video_recording()

    def on_feedback(self, future):
        if future.exception():
            log.error("video feed save stopped due to i/o error", exc_info=future.exc_info())
            self.server.stop_video_recording()

