from __future__ import print_function
from socket import socket, AF_INET, SOCK_DGRAM
from tornado.ioloop import IOLoop
from tornado.gen import coroutine, TracebackFuture
import logging
log = logging.getLogger(__name__)


class UDPServer(object):
    def __init__(self, bind, in_ioloop=None):
        self.socket = socket(AF_INET, SOCK_DGRAM)
        self.socket.setblocking(False)
        log.info("listening on %r for UDP dataframes", bind)
        self.socket.bind(bind)
        self._state = None
        self._read_callback = None
        self._read_timeout = None
        self.ioloop = in_ioloop or IOLoop.current()

    def _add_io_state(self, state):
        if self._state is None:
            self._state = IOLoop.ERROR | state
            self.ioloop.add_handler(
                self.socket.fileno(), self._handle_events, self._state)
        elif not self._state & state:
            self._state = self._state | state
            self.ioloop.update_handler(self.socket.fileno(), self._state)

    def sendto(self, msg, addr):
        return self.socket.sendto(msg, addr)

    def close(self):
        log.info("UDP listener closed")
        self.ioloop.remove_handler(self.socket.fileno())
        self.socket.close()
        self.socket = None

    def read_chunk(self, timeout=None):
        future = TracebackFuture()
        self._read_callback = future.set_result
        if timeout is not None:
            self._read_timeout = self.ioloop.add_timeout( self.ioloop.time() + timeout,
                self.check_read_callback )
        self._add_io_state(self.ioloop.READ)
        return future

    def check_read_callback(self):
        if self._read_callback:
            # XXX close socket?
            self._read_callback(None, error='timeout');

    def _handle_read(self):
        if self._read_timeout is not None:
            self.ioloop.remove_timeout(self._read_timeout)
            self._read_timeout = None
        if self._read_callback:
            try:
                data = self.socket.recvfrom(4096)
            except:
                # conn refused??
                data = None
            self._read_callback(data);
            self._read_callback = None

    def _handle_events(self, fd, events):
        if events & self.ioloop.READ:
            self._handle_read()
        if events & self.ioloop.ERROR:
            log.error('%s event error', self)


if __name__ == '__main__':
    @coroutine
    def cose():
        s = UDPServer(("0.0.0.0", 6789))
        while True:
            data, addr = yield s.read_chunk()
            s.sendto(data, addr)
            print("data:", repr(data))

    io_loop = IOLoop.instance()
    io_loop.add_callback(cose)
    io_loop.start()