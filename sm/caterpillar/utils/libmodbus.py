import warnings
from os import environ
import logging
from time import sleep

from six import binary_type
from tornado.options import options
from threading import RLock

log = logging.getLogger(__name__)

ENABLE_DEBUG = environ.get("ENABLE_MODBUS_DEBUG", "0") != "0"

try:
    """
    import _sm_caterpillar_libmodbus
    from sm.caterpillar import libmodbus as libmodbus_native

    def modbus_new_rtu(device, baud, parity, data_bit, stop_bit):
        if isinstance(device, binary_type):
            device = str(device, "ascii")
        if isinstance(parity, binary_type):
            parity = str(parity, "ascii")
        return libmodbus_native.modbus_new_rtu(device, baud, parity, data_bit, stop_bit)

    modbus_set_slave = libmodbus_native.modbus_set_slave
    modbus_write_register = libmodbus_native.modbus_write_register
    modbus_close = libmodbus_native.modbus_close
    modbus_free = libmodbus_native.modbus_free
    modbus_connect = libmodbus_native.modbus_connect
    modbus_read_registers = libmodbus_native.modbus_read_registers
    """

    from minimalmodbus import Instrument, _SERIALPORTS
    from serial import Serial

    def modbus_new_rtu(device, baud, parity, bdata, bstop):
        if isinstance(device, binary_type):
            device = str(device, "ascii")
        if isinstance(parity, binary_type):
            parity = str(parity, "ascii")
        _SERIALPORTS[device] = Serial(port=device, baudrate=baud, parity=parity, bytesize=bdata, stopbits=bstop, timeout=1)
        return Instrument(device, slaveaddress=1)

    def modbus_set_slave(h, address):
        h.address = address

    def modbus_write_register(h, reg, data):
        h.write_register(reg, data&0xffff, functioncode=6)

    def modbus_close(h):
        pass

    def modbus_free(h):
        pass

    def modbus_connect(h):
        pass

    def modbus_read_registers(h, addr, size):
        return h.read_registers(addr, size)

except ImportError:
    warnings.warn("unable to load MinimalModbus and PySerial libs. "
                  "Failback to ctypes-based libmodbus wrapper (unreliable)")

    from ctypes import cdll, c_char_p, c_int, c_char, c_void_p, c_uint16, byref

    libmodbus = cdll.LoadLibrary("libmodbus.so")

    modbus_new_rtu = libmodbus.modbus_new_rtu
    modbus_new_rtu.argtypes=[c_char_p, c_int ,c_char ,c_int ,c_int]
    modbus_new_rtu.restype = c_void_p

    modbus_set_slave = libmodbus.modbus_set_slave
    modbus_set_slave.argtypes = [c_void_p, c_int]
    modbus_set_slave.restype = c_int



    modbus_write_register = libmodbus.modbus_write_register
    modbus_write_register.argtypes = [c_void_p, c_int, c_int]
    modbus_write_register.restype = c_int

    modbus_close = libmodbus.modbus_close
    modbus_close.argtypes = [ c_void_p ]

    modbus_free = libmodbus.modbus_free
    modbus_free.argtypes = [ c_void_p ]

    modbus_connect = libmodbus.modbus_connect
    modbus_connect.argtypes = [c_void_p]
    modbus_connect.restype = c_int

    modbus_read_registers = libmodbus.modbus_read_registers
    modbus_read_registers.argtypes = [c_void_p, c_int, c_int, c_void_p]
    modbus_read_registers.restype = c_int


if ENABLE_DEBUG:
    def wrap(f):
        def wrapper(*a):
            log.debug("%s%r", f.__name__, a)
            res = f(*a)
            log.debug("res = %r", res)
            return res
        return wrapper


    modbus_new_rtu = wrap(modbus_new_rtu)
    modbus_set_slave = wrap(modbus_set_slave)
    modbus_write_register = wrap(modbus_write_register)
    modbus_close = wrap(modbus_close)
    modbus_free = wrap(modbus_free)
    modbus_connect = wrap(modbus_connect)
    modbus_read_registers = wrap(modbus_read_registers)


MODBUS_RTU_RS485 = 1
MODBUS_RTU_RTS_NONE  = 0
MODBUS_RTU_RTS_UP    = 1
MODBUS_RTU_RTS_DOWN  = 2


class Bus:
    def __init__(self, port=b"/dev/ttyUSB0", baud=115200, parity=b"N", dbits=8, sbits=1, verify_time=0.001):
        if not isinstance(port, binary_type):
            port = binary_type(port, "ascii")
        if not isinstance(parity, binary_type):
            parity = binary_type(parity, "ascii")
        self.port = port
        self.baud = baud
        self.par = parity
        self.dbits = dbits
        self.sbits = sbits
        self.last_slave = None
        self.verify_time = verify_time
        if options.noop:
            self.set_slave = self.set_slave_noop
            self.close = self.close_noop
            self.write = self.write_noop
            self.read = self.read_noop
        else:
            self.h = modbus_new_rtu(port, baud, parity, dbits, sbits)
            if not self.h:
                raise RuntimeError("unable to create modbus context: %s" % repr((port, baud, parity, dbits, sbits)))
        self._mutex = RLock()

    def set_slave(self, slave):
        with self._mutex:
            log.debug("modbus: set_slave %r", slave)
            if modbus_set_slave(self.h, slave) == -1:
                raise RuntimeError("unable to set modbus slave %r" % slave)
            if modbus_connect(self.h) == -1:
                raise RuntimeError("unable to connect modbus context")
            self.last_slave = slave

    def set_slave_noop(self, slave):
        log.debug("modbus: set_slave %r", slave)

    def close(self):
        with self._mutex:
            log.debug("modbus: close")
            modbus_close(self.h)
            modbus_free(self.h)
            self.h = None

    def close_noop(self):
        log.debug("modbus: close")

    def write(self, slave, addr, val):
        with self._mutex:
            log.debug("modbus: write 0x%04x@%r: 0x%04x", addr, slave, val)
            if self.last_slave != slave:
                self.set_slave(slave)
            if options.no_modbus_read:
                modbus_write_register(self.h, addr, val)
                return

            if modbus_write_register(self.h, addr, val) == -1:
                raise RuntimeError("unable to write to slave %r at address 0x%04x, value = 0x%04x" % (slave, addr, val))
            if self.verify_time:
                sleep(self.verify_time)
                res = self.read(slave, addr)
                if res != (val&0xffff):
                    raise RuntimeError("unable to write to slave %r at address 0x%04x, "
                                       "expected=0x%04x, read==0x%04x" % (slave, addr, val, res))

    def write_noop(self, slave, addr, val):
        log.debug("modbus: write 0x%04x@%r: 0x%04x", addr, slave, val)
        sleep(0.2)

    def read(self, slave, addr):
        with self._mutex:
            log.debug("modbus: read 0x%04x@%r ...", addr, slave)
            if self.last_slave != slave:
                self.set_slave(slave)
            box = c_uint16()
            res = modbus_read_registers(self.h, addr, 1, byref(box))
            if res != 1:
                raise RuntimeError("unable to read from slave %r at address 0x%04x, read_blocks=%d" % (slave, addr, res))
            log.debug("0x%04x@%r read value: 0x%04x", box.value)
            return box.value

    def read_noop(self, slave, addr):
        log.debug("modbus: read 0x%04x@%r", addr, slave)



if __name__ == '__main__':
    bus = Bus()
    bus.write(1, 0x6500, 0)
    bus.close()
