def singleton(cls):
    """Decorator that add a "instance()" static method to a class, observing a singleton pattern"
    """
    @staticmethod
    def instance():
        if not hasattr(cls, "_instance"):
            cls._instance = cls()
        return cls._instance
    cls.instance = instance
    return cls


