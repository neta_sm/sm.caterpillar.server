import logging
from threading import Thread

from os import environ
from tornado.gen import coroutine
from tornado.ioloop import IOLoop
from tornado.options import options, define

from ..server.commands import (
    Factory,
    GetVersion,
    Power12VRailEnable,
    Response,
    InitCommand,
    EnableCommand,
    SetSpeed,
    SetVideoFeed,
    SetCameraLED,
    LEDPulse,
)
from ..server.commons import DEF_DRIVER_ARAMP, DEF_DRIVER_DRAMP, DEF_DRIVER_MAXRPM
from ..udp import UDPServer


log = logging.getLogger(__name__)

define("host", default=environ.get("CATERPILLAR_HOST", "192.168.68.1"), type=str)
define("port", default=environ.get("CATERPILLAR_PORT", 3135), type=int)
define("timeout", default=5, type=int)


class RemoteControl(Thread):
    def __init__(self, *a, **ka):
        super(RemoteControl, self).__init__(*a, **ka)
        self.daemon = True
        self._io_loop = None
        self.addr = (options.host, options.port)
        self.udp = UDPServer(("0.0.0.0", options.port+1))
        self.command = Factory()
        self.over = False

    @property
    def io_loop(self):
        if self._io_loop is None:
            self._io_loop = IOLoop.current()
        return self._io_loop

    def run(self):
        self.io_loop.add_callback(self._read_loop)
        self.io_loop.start()

    def stop(self):
        self.io_loop.stop()

    @coroutine
    def _send(self, cmd):
        log.debug("sending command %r", cmd)
        data = cmd.toData()
        log.debug("sending string of data to %r: %r", self.addr, data)
        self.udp.sendto(data, self.addr)

    @coroutine
    def _read_loop(self):
        while not self.over:
            data, addr = yield self.udp.read_chunk()
            log.debug("got %d bytes of data from %r: %r", len(data), addr, data)
            res = Response.fromData(data)
            log.debug("response object: %r", res)

    def get_version(self):
        cmd = self.command(GetVersion)
        self.io_loop.add_callback(self._send, cmd)

    def enable_12v(self):
        cmd = self.command(Power12VRailEnable, active=1)
        self.io_loop.add_callback(self._send, cmd)

    def disable_12v(self):
        cmd = self.command(Power12VRailEnable, active=0)
        self.io_loop.add_callback(self._send, cmd)

    def init_driver(self, ndriver, max_rpm=DEF_DRIVER_MAXRPM, aramp=DEF_DRIVER_ARAMP, dramp=DEF_DRIVER_DRAMP):
        cmd = self.command(InitCommand, driver=ndriver, accel_ramp=aramp, decel_ramp=dramp, max_rpm=max_rpm)
        self.io_loop.add_callback(self._send, cmd)

    def enable_driver(self, ndriver, active=1):
        cmd = self.command(EnableCommand, driver=ndriver, active=active)
        self.io_loop.add_callback(self._send, cmd)

    def set_speed(self, ndriver, rpm):
        cmd = self.command(SetSpeed, driver=ndriver, speed=rpm)
        self.io_loop.add_callback(self._send, cmd)

    def set_leds(self, *leds):
        mask = 0x0
        for led in leds:
            mask = mask | (1 << (led-1))
        cmd = self.command(SetCameraLED, on_mask=mask)
        self.io_loop.add_callback(self._send, cmd)

    def set_video_feed(self,
                         camera=0,
                         active=1,
                         flip_vert=False,
                         flip_horiz=False,
                         auto_mode=True,
                         resolution=None,
                         framerate=None,
                         shutter_speed=None,
                         iso=None,
                         settle_time=None,
                         record=False,
                         ):
        width=0
        height=0
        if resolution:
            width, height = resolution
        flip_xy = 0x0 | (0x1 if flip_vert else 0x0) | (0x2 if flip_horiz else 0x0)
        fps_num = 0
        fps_den = 0
        if framerate:
            fps_num = framerate
            fps_den = 1
        shutter_num = 0
        shutter_den = 0
        if shutter_speed:
            shutter_num, shutter_den = shutter_speed
        args = dict(active=active, auto_mode=auto_mode,
                    camera=camera, flip_xy=flip_xy,
                    width=width, height=height,
                    fps_num=fps_num, fps_den=fps_den,
                    shutter_num=shutter_num, shutter_den=shutter_den,
                    iso=iso if iso else 0xff,
                    settle_time_s=settle_time if settle_time else 0xff,
                    record=record,
                    )
        cmd = self.command(SetVideoFeed, **args)
        self.io_loop.add_callback(self._send, cmd)

    def led_pulse(self, mask=0x01, pulses=1, delay_ms=500, duration_ms=500):
        cmd = self.command(LEDPulse, led_mask=mask, led_pulses=pulses,
                           led_pulse_len_ms=duration_ms, led_pulse_del_ms=delay_ms)
        self.io_loop.add_callback(self._send, cmd)
