from sm.caterpillar.server.server import Server
from tornado.ioloop import IOLoop
import logging
log = logging.getLogger(__name__)


if __name__ == '__main__':
    from tornado.options import parse_command_line
    parse_command_line()

    server = Server()

    io_loop = IOLoop.instance()
    io_loop.add_callback(server.serve_loop)
    io_loop.start()
