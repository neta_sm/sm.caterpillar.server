from IPython.terminal.embed import (
    load_default_config,
    InteractiveShell,
    InteractiveShellEmbed,
)
import sys

from tornado.gen import coroutine, sleep


def start_repl_here(local_env=None):
    """Call this to embed IPython at the current point in your program.

    :param local_env: object whose attributes will be visible in the REPL
    """
    kwargs = {}
    header = u""
    compile_flags = None
    config = load_default_config()
    config.InteractiveShellEmbed = config.TerminalInteractiveShell
    config.TerminalInteractiveShell.confirm_exit = False
    config.TerminalInteractiveShell.banner1 = u""
    config.TerminalInteractiveShell.in_template = u"Redirector[\\#] :"
    kwargs['config'] = config
    #save ps1/ps2 if defined
    ps1 = None
    ps2 = None
    try:
        ps1 = sys.ps1
        ps2 = sys.ps2
    except AttributeError:
        pass
    #save previous instance
    saved_shell_instance = InteractiveShell._instance
    if saved_shell_instance is not None:
        cls = type(saved_shell_instance)
        cls.clear_instance()
    shell = InteractiveShellEmbed.instance(**kwargs)
    shell(header=header, stack_depth=2, compile_flags=compile_flags, local_ns=local_env)
    InteractiveShellEmbed.clear_instance()
    #restore previous instance
    if saved_shell_instance is not None:
        cls = type(saved_shell_instance)
        cls.clear_instance()
        for subclass in cls._walk_mro():
            subclass._instance = saved_shell_instance
    if ps1 is not None:
        sys.ps1 = ps1
        sys.ps2 = ps2

@coroutine
def sequence(control, io_loop):
#    control.get_version()
#    yield sleep(1)
#    control.disable_12v()
#    yield sleep(1)
#    control.enable_12v()
#    yield sleep(1)
#    control.init_driver(0)
    #control.enable_driver(0, 0)
    ndr=1
    control.init_driver(ndr)
    control.init_driver(ndr+1)
    yield sleep(1)
    return
    control.enable_driver(ndr)
    yield sleep(1)
    control.set_speed(ndr, 1000)
    yield sleep(2)
    control.set_speed(ndr, 2000)
    yield sleep(2)
    control.set_speed(ndr, 3000)
    yield sleep(2)
    control.enable_driver(ndr, False)
    yield sleep(1)
    io_loop.stop()

if __name__ == '__main__':
    from sm.caterpillar.control import RemoteControl
    from tornado.ioloop import IOLoop
    from tornado.options import parse_command_line
    parse_command_line()
    control = RemoteControl()
    control.start()
    start_repl_here(local_env={"caterpillar": control})
    control.stop()
    control.join()
